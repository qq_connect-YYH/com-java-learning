#### 介绍
[参考程序猿DD 做的笔记 ^_^ ](http://blog.didispace.com/spring-boot-learning-2x/)

#### 软件架构
0. springboot 相关学习  com-springboot-learning
 
1. nacos的学习 com-nacos-root
 
2. 数据库相关的学习 com-spring-database-learning
 
3.日志相关学习 com-spring-logs-learning
 
4. 消息相关学习 com-spring-msg-service-learning
 
5. security 相关学习 spring-boot-starter-parent
 
6. state machine 框架的学习 com-spring-statemachine-learning
 
7. 定时和异步任务的学习 com-spring-timingandasyn-learning
 
8. spring cloud dalston 相关学习 spring-cloud-dalston-series
 
9. spring cloud greenwich版本学习 spring-cloud-greenwich-series




