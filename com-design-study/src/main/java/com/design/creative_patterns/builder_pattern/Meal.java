package com.design.creative_patterns.builder_pattern;

import lombok.Data;

/**
 * 套餐
 * Created by Administrator on 2019/8/18.
 */
@Data
public class Meal {
    private String food;
    private String drink;
}
