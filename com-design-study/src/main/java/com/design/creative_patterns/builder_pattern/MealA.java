package com.design.creative_patterns.builder_pattern;

/**
 * 实现抽象接口，构建和装配各个部件。
 * Created by Administrator on 2019/8/18.
 */
public class MealA extends MealBuilder {

    public void buildDrink() {
        meal.setDrink("可乐");
    }

    public void buildFood() {
        meal.setFood("薯条");
    }

}
