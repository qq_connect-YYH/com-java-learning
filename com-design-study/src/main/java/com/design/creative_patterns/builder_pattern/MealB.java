package com.design.creative_patterns.builder_pattern;

/**
 * Created by Administrator on 2019/8/18.
 */
public class MealB extends MealBuilder {

    public void buildDrink() {
        meal.setDrink("柠檬果汁");
    }

    public void buildFood() {
        meal.setFood("鸡翅");
    }
}
