package com.design.creative_patterns.builder_pattern;

/**
 * 创建一个Product对象的各个部件指定的抽象接口。
 * Created by Administrator on 2019/8/18.
 */
public abstract class MealBuilder {
    Meal meal = new Meal();

    public abstract void buildFood();

    public abstract void buildDrink();

    public Meal getMeal(){
        return meal;
    }

}
