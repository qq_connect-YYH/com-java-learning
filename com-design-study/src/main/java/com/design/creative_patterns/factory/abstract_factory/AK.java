package com.design.creative_patterns.factory.abstract_factory;

/**
 * Created by Administrator on 2019/8/18.
 */
public class AK implements Gun {

    @Override
    public void shooting() {
        System.out.println("shooting with AK");
    }
}
