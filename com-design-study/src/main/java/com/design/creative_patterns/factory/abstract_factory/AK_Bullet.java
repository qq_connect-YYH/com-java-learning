package com.design.creative_patterns.factory.abstract_factory;

/**
 * Created by Administrator on 2019/8/18.
 */
public class AK_Bullet implements Bullet {

    @Override
    public void load() {
        System.out.println("Load bullets with AK");
    }
}
