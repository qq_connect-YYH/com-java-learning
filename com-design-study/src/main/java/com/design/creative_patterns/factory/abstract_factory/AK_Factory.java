package com.design.creative_patterns.factory.abstract_factory;

/**
 * Created by Administrator on 2019/8/18.
 */
public class AK_Factory implements Factory {
    @Override
    public Gun produceGun() {
        return new AK();
    }

    @Override
    public Bullet produceBullet() {
        return new AK_Bullet();
    }
}
