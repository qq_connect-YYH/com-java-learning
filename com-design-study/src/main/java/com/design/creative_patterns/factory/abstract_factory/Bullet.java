package com.design.creative_patterns.factory.abstract_factory;

/**
 * 子弹
 * Created by Administrator on 2019/8/18.
 */
public interface Bullet {

    void load();

}
