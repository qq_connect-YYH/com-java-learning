package com.design.creative_patterns.factory.abstract_factory;

/**
 * 抽象工厂
 * Created by Administrator on 2019/8/18.
 */
public interface Factory {

    Gun produceGun();

    Bullet produceBullet();

}
