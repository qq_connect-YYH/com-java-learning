package com.design.creative_patterns.factory.abstract_factory;

/**
 * Created by Administrator on 2019/8/18.
 */
public class M4A1_Bullet implements Bullet {

    @Override
    public void load() {
        System.out.println("Load bullets with M4A1");
    }
}
