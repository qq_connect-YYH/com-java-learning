package com.design.creative_patterns.factory.abstract_factory;

/**
 * Created by Administrator on 2019/8/18.
 */
public class M4A1_Factory implements Factory {

    @Override
    public Gun produceGun() {
        return new M4A1();
    }

    @Override
    public Bullet produceBullet() {
        return new M4A1_Bullet();
    }
}
