package com.design.creative_patterns.factory.abstract_factory;

/**
 * 抽象工厂测试
 * Created by Administrator on 2019/8/18.
 */
public class Test {

    public static void main(String[] args) {
        Factory factory;
        Gun gun;
        Bullet bullet;

        factory = new AK_Factory();
        bullet = factory.produceBullet();
        bullet.load();
        gun = factory.produceGun();
        gun.shooting();

    }
}
