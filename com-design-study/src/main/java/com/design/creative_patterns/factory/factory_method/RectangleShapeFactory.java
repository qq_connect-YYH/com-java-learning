package com.design.creative_patterns.factory.factory_method;

import com.design.creative_patterns.factory.simple_factory.AbstractShape;
import com.design.creative_patterns.factory.simple_factory.Rectangle;

/**
 * 具体工厂实现类
 * Created by Administrator on 2019/8/18.
 */
public class RectangleShapeFactory implements ShapeFactory {
    @Override
    public AbstractShape getShape() {
        return new Rectangle();
    }
}
