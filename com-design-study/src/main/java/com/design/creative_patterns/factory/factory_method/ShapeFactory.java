package com.design.creative_patterns.factory.factory_method;

import com.design.creative_patterns.factory.simple_factory.AbstractShape;

/**
 * 抽象工厂
 * Created by Administrator on 2019/8/18.
 */
public interface ShapeFactory {

    AbstractShape getShape();

}
