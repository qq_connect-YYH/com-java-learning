package com.design.creative_patterns.factory.factory_method;

import com.design.creative_patterns.factory.simple_factory.AbstractShape;
import com.design.creative_patterns.factory.simple_factory.Square;

/**
 * Created by Administrator on 2019/8/18.
 */
public class SquareShapeFactory implements ShapeFactory {

    @Override
    public AbstractShape getShape() {
        return new Square();
    }
}
