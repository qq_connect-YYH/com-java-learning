package com.design.creative_patterns.factory.factory_method;

import com.design.creative_patterns.factory.simple_factory.Shape;

/**
 * 工厂方法测试
 * Created by Administrator on 2019/8/18.
 */
public class Test {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new CircleShapeFactory();
        Shape circle = shapeFactory.getShape();
        circle.draw();
    }
}
