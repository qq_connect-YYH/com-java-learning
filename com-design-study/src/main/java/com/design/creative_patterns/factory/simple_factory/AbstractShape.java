package com.design.creative_patterns.factory.simple_factory;

/**
 * Created by Administrator on 2019/8/18.
 */
public abstract class AbstractShape implements Shape {

    @Override
    public void draw() {
        System.out.println("默认抽象类");
    }

}
