package com.design.creative_patterns.factory.simple_factory;

/**
 * 圆形
 * Created by Administrator on 2019/8/18.
 */
public class Circle extends AbstractShape {
    @Override
    public void draw() {
        System.out.println("draw circle");
    }
}
