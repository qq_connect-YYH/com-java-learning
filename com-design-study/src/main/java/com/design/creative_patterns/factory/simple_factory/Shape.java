package com.design.creative_patterns.factory.simple_factory;

/**
 * 创建一个可以绘制不同形状的绘图工具，可以绘制圆形，正方形，三角形，每个图形都会有一个draw()方法用于绘图.
 * Created by Administrator on 2019/8/18.
 */
public interface Shape {

    void draw();
}
