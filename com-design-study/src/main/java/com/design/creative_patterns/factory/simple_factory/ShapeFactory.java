package com.design.creative_patterns.factory.simple_factory;

/**
 * 工厂类
 * Created by Administrator on 2019/8/18.
 */
public class ShapeFactory {

    /**
     * 通过字符串判断该实例化哪个类
     * 弊端：拓展了一个新类需要修改这个方法，这违反了开闭原则了
     * @param shapeType
     * @return
     */
    public static AbstractShape getDraw(String shapeType){
        if (shapeType == null) {
            return null;
        }
        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle();
        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle();
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            return new Square();
        }
        return null;
    }

    /**
     * 通过放射获取对象
     * 这种方式的虽然符合了 开放-关闭原则 ，但是每一次传入的都是产品类的全部路径，这样比较麻烦。
     * 如果需要改善的话可以通过 反射+ 配置文件 的形式来改善，这种方式使用的也是比较多的。
     * @param clazz
     * @return
     */
    public static Object getDraw(Class<?> clazz){
        try {
            return Class.forName(clazz.getName()).newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }
}
