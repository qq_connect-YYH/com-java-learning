package com.design.creative_patterns.factory.simple_factory;

/**
 * 正方形
 * Created by Administrator on 2019/8/18.
 */
public class Square extends AbstractShape {
    @Override
    public void draw() {
        System.out.println("draw square");
    }
}
