package com.design.creative_patterns.factory.simple_factory;

/**
 * 简单工厂模式测试
 * Created by Administrator on 2019/8/18.
 */
public class Test {

    public static void main(String[] args) {
//        test1();
        AbstractShape circle = (AbstractShape) ShapeFactory.getDraw(Circle.class);
        circle.draw();
        AbstractShape rectangle = (AbstractShape) ShapeFactory.getDraw(Rectangle.class);
        rectangle.draw();
        AbstractShape square = (AbstractShape) ShapeFactory.getDraw(Square.class);
        square.draw();
    }

    private static void test1() {
        // TODO: 2019/8/18 这样的实现有个问题，如果我们新增产品类的话，就需要修改工厂类中的getShape（）方法，这很明显不符合 开放-封闭原则
        // 获取 Circle 的对象，并调用它的 draw 方法
        AbstractShape circle = ShapeFactory.getDraw("CIRCLE");
        circle.draw();
        // 获取 Rectangle 的对象，并调用它的 draw 方法
        AbstractShape rectangle = ShapeFactory.getDraw("RECTANGLE");
        rectangle.draw();
        // 获取 Square 的对象，并调用它的 draw 方法
        AbstractShape square = ShapeFactory.getDraw("SQUARE");
        square.draw();
    }
}
