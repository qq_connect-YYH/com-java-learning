package com.design.creative_patterns.prototype_pattern;

/**
 * Created by Administrator on 2019/8/18.
 */
public class Test {

    public static void main(String[] args) {
        UnderlinePen underlinePen = new UnderlinePen('~');
        MessageBox mbox = new MessageBox('*');
        MessageBox sbox = new MessageBox('/');

        Manager manager = new Manager();
        manager.register("Strong message", underlinePen);
        manager.register("Waring Box", mbox);
        manager.register("Slash Box", sbox);

        Product p1 = manager.create("Strong message");
        p1.use("hello world");
        Product p2 = manager.create("Waring Box");
        p2.use("hello world");
        Product p3 = manager.create("Slash Box");
        p3.use("hello world");
    }

}
