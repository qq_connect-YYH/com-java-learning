package com.design.creative_patterns.singleton;

/**
 * 饿汉型的单例模式
 * 所谓 “饿汉方式” 就是说JVM在加载这个类时就马上创建此唯一的单例实例，不管你用不用，先创建了再说，
 * 如果一直没有被使用，便浪费了空间，典型的空间换时间，每次调用的时候，就不需要再判断，节省了运行时间。
 * Created by Administrator on 2019/8/18.
 */
public class Singleton1E {
    //在静态初始化器中创建单例实例，这段代码保证了线程安全
    private static final Singleton1E instance = new Singleton1E();
    //Singleton类只有一个构造方法并且是被private修饰的，所以用户无法通过new方法创建该对象实例
    private Singleton1E(){}
    public static Singleton1E getInstance(){
        return instance;
    }
}
