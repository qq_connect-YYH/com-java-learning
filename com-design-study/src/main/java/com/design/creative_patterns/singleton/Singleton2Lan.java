package com.design.creative_patterns.singleton;

/**
 * 懒汉型的单例模式
 * Created by Administrator on 2019/8/18.
 */
public class Singleton2Lan {
    //加入volatile保证数据的可见性，直接判断堆是否存在该对象
    //volatile保证，当uniqueInstance变量被初始化成Singleton实例时，多个线程可以正确处理uniqueInstance变量
    private volatile static Singleton2Lan instance ;
    //Singleton类只有一个构造方法并且是被private修饰的，所以用户无法通过new方法创建该对象实例
    private Singleton2Lan(){}

    public static Singleton2Lan getInstance(){
        //检查实例，如果不存在，就进入同步代码块
        if(instance == null){
            //没有加入synchronized关键字的版本是线程不安全的，加入了是线程安全的
            synchronized (Singleton2Lan.class){
                if(instance == null){
                    instance = new Singleton2Lan();
                }
            }
        }
        return instance;
    }
}
