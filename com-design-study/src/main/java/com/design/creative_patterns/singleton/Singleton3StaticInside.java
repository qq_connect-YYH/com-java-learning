package com.design.creative_patterns.singleton;

/**
 * 懒汉型（登记式/静态内部类方式） 静态内部实现的单例是懒加载的且线程安全
 * Created by Administrator on 2019/8/18.
 */
public class Singleton3StaticInside {
    private Singleton3StaticInside(){}
    public static Singleton3StaticInside getInstance(){
        return SingleHolder.OBJ;
    }

    /**
     * 只有通过显式调用 getInstance 方法时，才会显式装载 SingletonHolder 类，
     * 从而实例化 instance（只有第一次使用这个单例的实例的时候才加载，同时不会有线程安全问题）。
     */
    private static class SingleHolder{
        private static final Singleton3StaticInside OBJ = new Singleton3StaticInside();
    }
}
