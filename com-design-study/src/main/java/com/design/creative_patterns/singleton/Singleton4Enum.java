package com.design.creative_patterns.singleton;

/**
 * 1. 这种方法在功能上与公有域方法相近，但是它更加简洁，无偿提供了序列化机制，绝对防止多次实例化，即使是在面对复杂序列化或者反射攻击的时候。
 *    虽然这种方法还没有广泛采用，但是单元素的枚举类型已经成为实现Singleton的最佳方法。 —-《Effective Java 中文版 第二版》
 *
 * 2. 《Java与模式》中，作者这样写道，使用枚举来实现单实例控制会更加简洁，而且无偿地提供了序列化机制，
 *    并由JVM从根本上提供保障，绝对防止多次实例化，是更简洁、高效、安全的实现单例的方式。
 * Created by Administrator on 2019/8/18.
 */
public enum  Singleton4Enum {
     INSTANCE;

     public static void test(){
         System.out.println("my static test");
     }

     public void test2(){
         System.out.println("my instance test2");
     }

     static class SingleTest{
         public static void main(String[] args) {
             Singleton4Enum.INSTANCE.test2();
             Singleton4Enum.test();
         }
     }
}
