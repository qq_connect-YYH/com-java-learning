package com.yyh.nacos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients //开启feign
public class NacosConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosConsumerApplication.class, args);
    }

    /***************使用RestTemplate 消费*******************/
    @Bean
    @LoadBalanced //Spring Cloud会将请求拦截下来，然后通过负载均衡器选出节点，并替换服务名部分为具体的ip和端口，从而实现基于服务名的负载均衡调用
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @RestController
    public class NacosController{
        @Autowired
        private LoadBalancerClient loadBalancerClient;
        @Autowired
        private RestTemplate restTemplate;
        @Value("${spring.application.name}")
        private String appName;

        @GetMapping("/echo/app-name")
        public String echoAppName(){
            //使用 LoadBalanceClient 和 RestTemolate 结合的方式来访问
            ServiceInstance serviceInstance = loadBalancerClient.choose("nacos-producer");
            //不加 @LoadBalanced 使用
//            String url = String.format("http://%s:%s/echo/%s",serviceInstance.getHost(),serviceInstance.getPort(),appName);
            String url = String.format("http://%s/echo/%s", "nacos-producer", appName);
            System.out.println("request url:"+url);
            return restTemplate.getForObject(url,String.class);
        }
    }

    /***************使用WebClient 消费*******************/
    @RestController
    static class WebClientController {
        @Value("${spring.application.name}")
        private String appName;
        @Autowired
        private WebClient.Builder webClientBuilder;
        @GetMapping("/echo/app-name/webflux")
        public Mono<String> test() {
            Mono<String> result = webClientBuilder.build()
                    .get()
                    .uri("http://nacos-producer/echo/"+ appName)
                    .retrieve()
                    .bodyToMono(String.class);
            return result;
        }
    }
    @Bean
    @LoadBalanced
    public WebClient.Builder loadBalancedWebClientBuilder() {
        return WebClient.builder();
    }


    /***************使用Feign 消费*******************/
    @RestController
    static class FeignController {
        @Value("${spring.application.name}")
        private String appName;
        @Autowired
        Client client;
        @GetMapping("/echo/app-name/feign")
        public String test() {
            String result = client.echo(appName);
            return "Return : " + result;
        }
    }
    @FeignClient("nacos-producer")
    interface Client {
        @GetMapping("/echo/{string}")
        String echo(@PathVariable String string);
    }

}
