package com.yyh.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ManyConfigMysqlApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(com.yyh.nacos.ManyConfigMysqlApplication.class, args);
//        String userName = applicationContext.getEnvironment().getProperty("user.name");
//        String userAge = applicationContext.getEnvironment().getProperty("user.age");
//        System.err.println("user name :"+userName+"; age: "+userAge);
    }

    //支持配置的动态更新
//    public static void main(String[] args) {
//        ConfigurableApplicationContext applicationContext = SpringApplication.run(com.yyh.nacos.ProviderApplication.class, args);
//        while(true) {
//            //当动态配置刷新时，会更新到 Enviroment中，因此这里每隔一秒中从Enviroment中获取配置
//            String userName = applicationContext.getEnvironment().getProperty("user.name");
//            String userAge = applicationContext.getEnvironment().getProperty("user.age");
//            System.err.println("user name :" + userName + "; age: " + userAge);
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    //可支持profile粒度的配置
//    public static void main(String[] args) {
//        ConfigurableApplicationContext applicationContext = SpringApplication.run(ProviderApplication.class, args);
//        while(true) {
//            String useLocalCache = applicationContext.getEnvironment().getProperty("useLocalCache");
//            String userName = applicationContext.getEnvironment().getProperty("user.name");
//            String userAge = applicationContext.getEnvironment().getProperty("user.age");
//            //获取当前部署的环境
//            String currentEnv = applicationContext.getEnvironment().getProperty("current.env");
////            System.err.println("in "+currentEnv+" enviroment; "+"user name :" + userName + "; age: " + userAge);
//            System.out.println("useLocalCache  "+ useLocalCache);
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
