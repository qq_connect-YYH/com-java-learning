package com.yyh.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Created by Administrator on 2019/3/31.
 */
public class TestClient {
    /**
     * 手动从配置中心获取信息
     * @param args
     */
    public static void main(String[] args) {
        String addr ="localhost";
        String dataId = "test";
        String group = "DEFAULT_GROUP";
        Properties pro = new Properties();
        pro.put("serverAddr", addr);
        try {
            ConfigService configService = NacosFactory.createConfigService(pro);
            String content = configService.getConfig(dataId, group, 5000);
            System.out.println("1->".concat(content));

            configService.addListener(dataId, group, new Listener() {
                @Override
                public Executor getExecutor() {
                    return null;
                }
                @Override
                public void receiveConfigInfo(String configInfo) {
                    System.out.println("currentTime:"+ new Date() +", receive:" + configInfo);
                }
            });
            int i = System.in.read();
        } catch (NacosException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
