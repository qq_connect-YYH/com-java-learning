package com.yyh.nacos.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//让这个类下的配置内容支持动态刷新，也就是当我们的应用启动之后，修改了Nacos中的配置内容之后，这里也会马上生效
@RefreshScope
public class TestController {
    @Value("${useLocalCache:}")
    private String useLocalCache;
    @Value("${actuator:}")
    private String actuator;
    @Value("${log:}")
    private String log;

    @GetMapping("/test")
    public String hello() {
        return String.format("useLocalCache：%s; actuator：%s; log：%s", useLocalCache, actuator, log) ;
    }


}
