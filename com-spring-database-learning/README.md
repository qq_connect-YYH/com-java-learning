### 数据访问层学习
1. [参考DD](http://blog.didispace.com/spring-boot-learning-1x/)

2. Spring Boot中使用JdbcTemplate访问数据库  
- `com.jdbc.ApplicationStart`

3. Spring Boot中使用Spring-data-jpa让数据访问更简单、更优雅
- `com.jpa.Application`
- [参考](http://blog.didispace.com/springbootdata2/)

4. 多数据源的配置
- jdbcTemplate: `com.jdbc.manysource.ManySourceApplication`
- jpa   `com.jpa.manydatasource.JpaApplication`   

5. Spring Boot中使用Redis数据库
- `com.redis.RedisApplication`

6. Spring Boot中使用MongoDB数据库
[官网](https://www.mongodb.com)
[参考DD](http://blog.didispace.com/springbootmongodb/)
```$xslt
常见的，我们可以直接用MongoDB来存储键值对类型的数据，如：验证码、Session等；由于MongoDB的横向扩展能力，
也可以用来存储数据规模会在未来变的非常巨大的数据，如：日志、评论等；
由于MongoDB存储数据的弱类型，也可以用来存储一些多变json数据，如：与外系统交互时经常变化的JSON报文。
而对于一些对数据有复杂的高事务性要求的操作，如：账户交易等就不适合使用MongoDB来存储
```

7. Spring Boot整合MyBatis Transaction
- `com.mybatis.MybatisApplication`
- 脏读、不可重复读、幻读 [参考](https://www.cnblogs.com/balfish/p/8298296.html)
```$xslt
脏读: 事务的脏读发生在一个事务A读取了被另一个事务B修改，但是还未提交的数据。假如B回退，则事务A读取的是无效的数据。这跟不可重复读类似，但是第二个事务不需要执行提交。 
不可重复读: 事务的不可重复读的重点是修改; 在事务A执行过程中，同样的条件, 你读取过的数据, 再次读取出来发现值不一样了。两次读取的中间该数据被B事务修改提交了导致的。
幻读: 事务的幻读的重点在于新增或者删除 (数据条数变化)。在事务A执行过程中，同样的条件, 第1次和第2次读出来的记录数不一样。两次读取的中间B事务新增、删除数据并提交了导致的。
```
- 事务隔离级别 @Transactional(isolation = Isolation.DEFAULT)
```$xslt
DEFAULT：这是默认值，表示使用底层数据库的默认隔离级别。对大部分数据库而言，通常这值就是：READ_COMMITTED。
READ_UNCOMMITTED：该隔离级别表示一个事务可以读取另一个事务修改但还没有提交的数据。该级别不能防止脏读和不可重复读，因此很少使用该隔离级别。
READ_COMMITTED：该隔离级别表示一个事务只能读取另一个事务已经提交的数据。该级别可以防止脏读，这也是大多数情况下的推荐值。
REPEATABLE_READ：【mysql默认级别】该隔离级别表示一个事务在整个过程中可以多次重复执行某个查询，并且每次返回的记录都相同。即使在多次查询之间有新增的数据满足该查询，这些新增的记录也会被忽略。该级别可以防止脏读和不可重复读。
SERIALIZABLE：所有的事务依次逐个执行，这样事务之间就完全不可能产生干扰，也就是说，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下也不会用到该级别。
```
- 事务的传播行为
```$xslt
REQUIRED：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。
SUPPORTS：如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。
MANDATORY：如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。
REQUIRES_NEW：创建一个新的事务，如果当前存在事务，则把当前事务挂起。
NOT_SUPPORTED：以非事务方式运行，如果当前存在事务，则把当前事务挂起。
NEVER：以非事务方式运行，如果当前存在事务，则抛出异常。
NESTED：如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于REQUIRED。
```


8. Spring Boot整合MyBatis 加入缓存EhCache
- 加入ehcache缓存
- `com.mybatiscache.EhCacheApplication`

9. Spring Boot整合MyBatis 加入缓存Redis
- `com.mybatisredis.MybatisRedisApplication`
