package com.java.jdbc;

import com.java.jdbc.conf.ConnectionUtil;
import org.junit.Test;

import java.sql.*;

/**
 * Created by Administrator on 2019/9/14.
 */
public class TestCRUD {

    @Test
    public void testInsertPoint(){
        //多条新增 如果报错，回滚到保存点
        Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement = null;
        Savepoint one = null;
        try {
            //设置不自动提交事务
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement("INSERT INTO test (name) VALUES (?) ");
            preparedStatement.setString(1, "test a");
            int i = preparedStatement.executeUpdate();

            //设置保存点，之前的会提交，后续的会回滚
            one = connection.setSavepoint("one");

            preparedStatement = connection.prepareStatement("INSERT INTO test (name) VALUES (?) ");
            //不赋值 这里会报错，回滚
//            preparedStatement.setString(1, "test b");
            i = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback(one);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }finally {
            try {
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testInsert() {
        Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            //设置不自动提交事务
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement("INSERT INTO test (name) VALUES (?) ");
            preparedStatement.setString(1, "test1");
            int i = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Test
    public void testUpdate(){
        Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement ;
        try {
            //设置不自动提交事务
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement("UPDATE test SET NAME = ? WHERE id = ? ");
            preparedStatement.setString(1, "test3");
            preparedStatement.setInt(2, 3);
            int i = preparedStatement.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Test
    public void testDel(){
        Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement ;
        try {
            //设置不自动提交事务
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement("DELETE FROM test WHERE id = ? ");
            preparedStatement.setInt(1, 4);
            int i = preparedStatement.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Test
    public void testQuery(){
        Connection connection = ConnectionUtil.getConnection();
        PreparedStatement preparedStatement ;
        try {
            preparedStatement = connection.prepareStatement("select * from test ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Object id = resultSet.getObject("id");
                Object name = resultSet.getObject("name");
                System.out.print(id +" ");
                System.out.print(name +" ");
                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
