package com.mybatis.mapper;

import com.mybatis.entity.Test;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by Administrator on 2019/9/15.
 */
public interface TestMapper {

    @Select("select * from test ")
    List<Test> selectAll();

    @Select("select * from test where id = #{id} ")
    Test selectById(Integer id);

    @Update("update test set name = #{name} where id = #{id} ")
    int updateById(@Param("name") String name, @Param("id") Integer id);
}
