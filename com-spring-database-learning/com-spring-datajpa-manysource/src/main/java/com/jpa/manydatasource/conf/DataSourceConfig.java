package com.jpa.manydatasource.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 *  多数据源的配置
 * @author Administrator
 * @date 2019/6/18
 */
@Configuration
public class DataSourceConfig {

    /*********数据源配置********/


    @Bean("primaryDataSource")           //不写 name 估计会默认使用方法名给bean
    @Primary
    @Qualifier("primaryDataSource")     //数据源的修饰名
    @ConfigurationProperties(prefix = "spring.datasource.primary")
    public DataSource primaryDataSource(){
        DataSource build = DataSourceBuilder.create().build();
        return build;
    }


    @Bean("secondaryDataSource")
    @Qualifier("secondaryDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.secondary")
    public DataSource secondaryDataSource(){
        DataSource build = DataSourceBuilder.create().build();
        return build;
    }
}
