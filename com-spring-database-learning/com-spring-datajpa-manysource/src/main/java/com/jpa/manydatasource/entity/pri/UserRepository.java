package com.jpa.manydatasource.entity.pri;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Administrator on 2019/6/18.
 */
public interface UserRepository extends JpaRepository<User, Long>{

}
