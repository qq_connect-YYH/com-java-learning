package com.jpa.manydatasource.entity.sec;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2019/6/18.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

}
