import com.jpa.manydatasource.JpaApplication;
import com.jpa.manydatasource.entity.pri.User;
import com.jpa.manydatasource.entity.pri.UserRepository;
import com.jpa.manydatasource.entity.sec.Student;
import com.jpa.manydatasource.entity.sec.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/18.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JpaApplication.class)
public class JpaDataSourceTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void test() throws Exception {

        userRepository.save(new User("aaa", 10, ""));
        userRepository.save(new User("bbb", 20, ""));
        userRepository.save(new User("ccc", 30, ""));
        userRepository.save(new User("ddd", 40, ""));
        userRepository.save(new User("eee", 50, ""));

        Assert.assertEquals(5, userRepository.findAll().size());

        studentRepository.save(new Student("o1", 1));
        studentRepository.save(new Student("o2", 2));
        studentRepository.save(new Student("o3", 3));

        Assert.assertEquals(3, studentRepository.findAll().size());

    }
}
