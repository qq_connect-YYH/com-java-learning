package com.jdbc.manysource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author Administrator
 * @date 2019/6/18
 */
@SpringBootApplication
public class ManySourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManySourceApplication.class, args);
    }
}
