import com.jdbc.manysource.ManySourceApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/18.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManySourceApplication.class)
public class ManyDataSourceTest {

    @Autowired
    @Qualifier("primaryJdbcTemplate")
    JdbcTemplate primaryJdbcTemplate;

    @Autowired
    @Qualifier("secondaryJdbcTemplate")
    JdbcTemplate secondaryJdbcTemplate;


    @Before
    public void before(){
        int update = primaryJdbcTemplate.update("DELETE FROM USER ");
        log.info("primaryJdbcTemplate-delete:"+ update);
        int update1 = secondaryJdbcTemplate.update("DELETE FROM student");
        log.info("secondaryJdbcTemplate-delete:" + update1);
    }

    @Test
    public void test(){
        // 往第一个数据源中插入两条数据
        primaryJdbcTemplate.update("insert into user(id,name,age,addr) values(?, ?, ?, ?)", 1, "aaa", 20, "addr1");
        primaryJdbcTemplate.update("insert into user(id,name,age,addr) values(?, ?, ?, ?)", 2, "bbb", 30, "addr2");

        // 往第二个数据源中插入一条数据，若插入的是第一个数据源，则会主键冲突报错
        secondaryJdbcTemplate.update("insert into student(id,name,age) values(?, ?, ?)", 1, "aaa", 20);

        // 查一下第一个数据源中是否有两条数据，验证插入是否成功
        Assert.assertEquals("2", primaryJdbcTemplate.queryForObject("select count(1) from user", String.class));

        // 查一下第一个数据源中是否有两条数据，验证插入是否成功
        Assert.assertEquals("1", secondaryJdbcTemplate.queryForObject("select count(1) from student", String.class));
    }
}
