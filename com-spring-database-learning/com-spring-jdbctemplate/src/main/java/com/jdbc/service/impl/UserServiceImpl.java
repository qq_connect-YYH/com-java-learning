package com.jdbc.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.jdbc.service.UserService;

/**
 * Created by Administrator on 2019/6/18.
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 新增一个用户
     *
     * @param name
     * @param age
     */
    @Override
    public void create(String name, Integer age) {
        int update = jdbcTemplate.update("INSERT INTO USER (NAME , AGE) VALUES (?, ?)", name, age);
        log.info("create:" , update);
    }

    /**
     * 根据name删除一个用户高
     *
     * @param name
     */
    @Override
    public void deleteByName(String name) {
        int update = jdbcTemplate.update("DELETE FROM USER WHERE NAME = ?", name);
        log.info("deleteByName:" , update);
    }

    /**
     * 获取用户总量
     */
    @Override
    public Integer getAllUsers() {
        Integer integer = jdbcTemplate.queryForObject("select count(1) from USER", Integer.class);
        return integer;
    }

    /**
     * 删除所有用户
     */
    @Override
    public int deleteAllUsers() {
        int update = jdbcTemplate.update("DELETE FROM USER");
        log.info("deleteAllUsers:" , update);
        return update;
    }
}
