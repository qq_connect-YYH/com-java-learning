import com.jdbc.ApplicationStart;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.jdbc.service.UserService;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class)

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(ApplicationStart.class)
public class ApplicationTests {

    @Autowired
    UserService userSerivce;

    @Before
    public  void befor(){
        int i = userSerivce.deleteAllUsers();
        System.out.println("删除："+ i);
    }

    @Test
    public void test() throws Exception {
        // 插入5个用户
        userSerivce.create("a", 1);
        userSerivce.create("b", 2);
        userSerivce.create("c", 3);
        userSerivce.create("d", 4);
        userSerivce.create("e", 5);

        // 查数据库，应该有5个用户
        Assert.assertEquals(5, userSerivce.getAllUsers().intValue());

        // 删除两个用户
        userSerivce.deleteByName("a");
        userSerivce.deleteByName("e");

        // 查数据库，应该有5个用户
        Assert.assertEquals(3, userSerivce.getAllUsers().intValue());

    }
}
