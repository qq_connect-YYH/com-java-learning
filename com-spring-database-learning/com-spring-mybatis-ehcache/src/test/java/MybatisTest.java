import com.alibaba.fastjson.JSONObject;
import com.mybatiscache.EhCacheApplication;
import com.mybatiscache.entity.User;
import com.mybatiscache.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2019/6/19.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EhCacheApplication.class)
public class MybatisTest {

    @Autowired
    UserMapper userMapper;



    /**
     * 在没有加入第三方的缓存jar时：加入事务后，在一个事务执行过程中，查询同一个方法，只会查询一次数据库，后面的好像也会取出上一次的查询缓存，保证了幻读不会发生
     */
    @Test
    @Transactional
    public void testHuanDu(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));
    }


    /**
     * 观察cacheManager中的缓存集users
     * 没有引入其他缓存框架默认使用的是什么呢？
     * 在没有引入ehcache也可以进行缓存
     */
    @Autowired
    CacheManager cacheManager;

    @Test
    public void testQuery(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        //打个断点  看cacheManager 缓存了什么
        all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        User byId = userMapper.findById(2L);
        System.out.println(JSONObject.toJSONString(byId));
    }



}
