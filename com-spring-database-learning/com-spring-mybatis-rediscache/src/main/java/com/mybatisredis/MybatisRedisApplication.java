package com.mybatisredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Created by Administrator on 2019/6/20.
 */
@EnableCaching  //启动缓存，做了redis配置，会以redis作为缓存
@SpringBootApplication
public class MybatisRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisRedisApplication.class, args);
    }
}
