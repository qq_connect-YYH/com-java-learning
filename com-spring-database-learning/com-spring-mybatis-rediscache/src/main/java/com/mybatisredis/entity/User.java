package com.mybatisredis.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/6/20.
 */
@Data
public class User implements Serializable{

    private Long id;
    private String name;
    private Integer age;
    private String addr;
}
