import com.alibaba.fastjson.JSONObject;
import com.mybatisredis.MybatisRedisApplication;
import com.mybatisredis.entity.User;
import com.mybatisredis.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


/**
 * Created by Administrator on 2019/6/19.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MybatisRedisApplication.class)
public class MybatisTest {

    @Autowired
    UserMapper userMapper;

    @Autowired
    CacheManager cacheManager;

    @Test
    public void testQuery(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        //打个断点  看cacheManager 缓存了什么
        all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        User byId = userMapper.findById(2L);
        System.out.println(JSONObject.toJSONString(byId));
    }



}
