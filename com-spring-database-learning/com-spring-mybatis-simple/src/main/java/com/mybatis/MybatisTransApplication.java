package com.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2019/6/19.
 */
@SpringBootApplication
public class MybatisTransApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisTransApplication.class, args);
    }
}
