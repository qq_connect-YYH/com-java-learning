import com.alibaba.fastjson.JSONObject;
import com.mybatis.MybatisTransApplication;
import com.mybatis.entity.User;
import com.mybatis.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2019/6/19.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MybatisTransApplication.class)
public class MybatisTest {

    @Autowired
    UserMapper userMapper;

    @Test
    @Rollback
    @Transactional(rollbackFor = Exception.class)   //加入事务
    public void test(){
        //新增
        User user = new User();
        user.setAge(1);
        user.setName("test");
        user.setAddr("addr aukey");
        int count = userMapper.insert(user);
        System.out.println("count:" + count);
        System.out.println(JSONObject.toJSONString(user));

        //查询
        List<User> result = userMapper.findByName("test");
        System.out.println(JSONObject.toJSONString(result));

        //手动知道异常
        int i = 1/0;

        //更新
        user = userMapper.findById(2L);
        System.out.println(JSONObject.toJSONString(user));
        user.setAddr("aukeys");
        count = userMapper.updateById(user);
        System.out.println("updateById:" + count);

        //删除
        count = userMapper.deleteById(1L);
        System.out.println("deleteById:" + count);
    }

    @Test
    public void test2(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));
    }

    /**
     * 验证mysql不可重复读
     * mysql默认级别 REPEATABLE_READ
     */
    @Test
    @Transactional
    public void testBuKeChongFuDu(){
        User user = userMapper.findById(17L);
        System.out.println(JSONObject.toJSONString(user));

        //打个断点 去数据库修改该记录，再次查询，还是之前的数据，避免了不可重复读的发生
        user = userMapper.findById(17L);
        System.out.println(JSONObject.toJSONString(user));

        //结论：在【当前事务】执行过程中2次读取到的数据是一样的，即时其他事务去修改数据

        //tag：当我第一次查完后，试着去手动删除该条数据，该删除不会马上执行，会等你当前这个事务执行完，删除才执行生效。
    }

    /**
     * 验证幻读
     *
     */
    @Test
    @Transactional
    public void testHuanDu(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        //打个断点 去数据库删除或新增一条记录，再次查询，导致了幻读的发生
        all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        //结论：在【当前事务】执行过程中2次读取到的数据条数是一样的，不会产生幻读.
        //tag:虽然理论上说3. Repeatable Read(重复读)隔离级别在mysql中可能引起幻读的问题，
        // 但是mysql InnoDB存储引擎和XtraDB存储引擎通过多版本并发控制技术已经帮我们使用者解决了可能存在的幻读问题。
        // 意即，mysql如果用InnoDB或者XtraDB存储引擎并设置隔离级别为Repeatable Read也不会存在幻读问题，也就是效果和可串行化一样，但是效率比可串行化高的多。

    }


    /**
     * 在没有加入第三方的缓存jar时：加入事务后，在一个事务执行过程中，查询同一个方法，只会查询一次数据库，后面的好像也会取出上一次的查询缓存，保证了幻读不会发生
     */
    @Test
    @Transactional
    public void testHuanDu2(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        //打个断点  看cacheManager 缓存了什么
        all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));
    }
}
