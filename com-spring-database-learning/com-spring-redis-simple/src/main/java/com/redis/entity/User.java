package com.redis.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/6/19.
 */
@Data
public class User implements Serializable{

    private static final long serialVersionUID = -1L;

    private String username;
    private Integer age;

    public User(String username, Integer age) {
        this.username = username;
        this.age = age;
    }

}
