import com.redis.util.SerializeUtil;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by Administrator on 2019/9/8.
 */
public class JedisTest {
    public static void main(String[] args) throws Exception {
        /*
        练习：一个IP1小时内，只能登陆5次，超过5次，禁止登陆
         */
        Jedis jedis = new Jedis("localhost", 6379, 3000);
        jedis.auth("redis123456");
        //选择数据库
        jedis.select(1);
        String ip ="192.168.1.100";
        for (int i=0;i<1; i++){
            byte[] hget = jedis.hget(ip.getBytes(), "login".getBytes());
            ArrayList<Long> arrayList = SerializeUtil.unSerialize(hget, ArrayList.class);
            System.out.println("result "+ arrayList);
            if(CollectionUtils.isEmpty(arrayList)){
                arrayList.add(System.currentTimeMillis());
                jedis.hset(ip.getBytes(), "login".getBytes(), SerializeUtil.serialize(arrayList));
            }else{

                if(arrayList.size() >= 5 && (System.currentTimeMillis() - arrayList.get(arrayList.size()-5)) < 10000){
                    System.out.println("不能再添加了 "+ arrayList);
                }else{
                    arrayList.add(System.currentTimeMillis());
                    jedis.hset(ip.getBytes(), "login".getBytes(), SerializeUtil.serialize(arrayList));
                }
            }
            jedis.expire(ip.getBytes(), 12);
        }
        jedis.incr("a");
        System.out.println(jedis.get("a"));
//        Transaction multi = jedis.multi();
//        List<Object> exec = multi.exec();
//        String discard = multi.discard();
//        Response<String> watch = multi.watch("");
    }
}
