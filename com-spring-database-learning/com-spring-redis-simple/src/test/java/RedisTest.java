import com.alibaba.fastjson.JSONObject;
import com.redis.RedisApplication;
import com.redis.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/19.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisApplication.class)
public class RedisTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisTemplate<String, User> redisTemplate;

    @Test
    public void test(){
        // 保存字符串
        stringRedisTemplate.opsForValue().set("redis", "test1");
        Assert.assertEquals("test1", stringRedisTemplate.opsForValue().get("redis"));
        System.out.println(stringRedisTemplate.opsForValue().get("redis"));
    }


    @Test
    public void testObject(){
        //对象转json保存
        User user = new User("Json", 10);
        String s = JSONObject.toJSONString(user);
        stringRedisTemplate.opsForValue().set("user-json", s);

        // 保存对象
        user = new User("超人", 20);
        redisTemplate.opsForValue().set(user.getUsername(), user);

        user = new User("蝙蝠侠", 30);
        redisTemplate.opsForValue().set(user.getUsername(), user);

        user = new User("蜘蛛侠", 40);
        redisTemplate.opsForValue().set(user.getUsername(), user);

        Assert.assertEquals(20, redisTemplate.opsForValue().get("超人").getAge().longValue());
        Assert.assertEquals(30, redisTemplate.opsForValue().get("蝙蝠侠").getAge().longValue());
        Assert.assertEquals(40, redisTemplate.opsForValue().get("蜘蛛侠").getAge().longValue());
    }
}
