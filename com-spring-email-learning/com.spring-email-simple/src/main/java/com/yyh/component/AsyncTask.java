package com.yyh.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2018/11/29.
 */
@Component
public class AsyncTask {
//    @Autowired
//    java.util.concurrent.ThreadPoolExecutor ThreadPoolExecutor;
    /**
     * myTaskAsynPool即配置线程池的方法名，此处如果不写自定义线程池的方法名，会使用默认的线程池
     * 第一种方式
     * @param i
     */
//    @Async("myTaskAsyncPool")
    public void doTask(int i){
        System.out.println("当前线程："+Thread.currentThread());
        System.out.println("Task" + i + "started.");
    }

    @Async
    public void doTask2(int i){
        Thread thread = Thread.currentThread();
//        if( ThreadPoolExecutor.getActiveCount() == 0){
//            System.out.println("当前线程:" + thread.getName() + ",不是当前线程不执行，数字：" + i);
//            return;
//        }
        if(!thread.getName().startsWith("MyExecutor2")){
            System.out.println("当前线程:" + thread.getName() + ",不是当前线程不执行，数字：" + i);
            return;
        }
        try {
            System.out.println("当前线程：" +i + "->" + Thread.currentThread());
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
