package com.yyh;

import org.apache.commons.collections.map.HashedMap;
import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ApplicationTests {

	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private VelocityEngine velocityEngine;

	@Test
	public void sendSimpleMail() throws Exception {

		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("domain@aukeys.com");
		message.setTo("yiyuanhui@aukeys.com");
		message.setSubject("主题：简单邮件");
		message.setText("测试邮件内容");

		mailSender.send(message);
	}

	@Test
	public void sendAttachmentsMail() throws Exception {
		//码后的文件名长度如果大于60并且splitLongParameters的值为true，encodeParameters的值为true，
		// 文件名就会被截取，想想编码后的值被截取是什么样子？
		//在发送的时候初始化splitLongParameters为false不截取：

		System.setProperty("mail.mime.splitlongparameters","false");
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
		helper.setFrom("domain@aukeys.com");
//		String[] users = "yiyuanhui@aukeys.com".split(",");
		helper.setTo("yiyuanhui@aukeys.com");
		helper.setSubject("主题：有附件");
		helper.setText("测试，有附件的邮件");
//		FileSystemResource file1 = new FileSystemResource(new File("20181129深圳查询超时未审批单据.csv"));
//		FileSystemResource file2 = new FileSystemResource(new File("20181129天津超时未审批单据.csv"));
//		helper.addAttachment("20181129深圳查询超时未审批单据", file1);
//		helper.addAttachment("20181129天津超时未审批单据", file2);

		File file = new File("C:\\Users\\Administrator\\Desktop\\20181129深圳查询超时未审批单据.csv");
		File file2 = new File("C:\\Users\\Administrator\\Desktop\\20181129天津超时未审批单据.csv");
		helper.addAttachment(file.getName(), file);
		helper.addAttachment(file2.getName(), file2);
		mailSender.send(mimeMessage);
	}

	@Test
	public void sendInlineMail() throws Exception {

		MimeMessage mimeMessage = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		helper.setFrom("dyc87112@qq.com");
		helper.setTo("dyc87112@qq.com");
		helper.setSubject("主题：嵌入静态资源");
		helper.setText("<html><body><img src=\"cid:weixin\" ></body></html>", true);

		FileSystemResource file = new FileSystemResource(new File("weixin.jpg"));
		helper.addInline("weixin", file);

		mailSender.send(mimeMessage);
	}

	@Test
	public void sendTemplateMail() throws Exception {

		MimeMessage mimeMessage = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		helper.setFrom("dyc87112@qq.com");
		helper.setTo("dyc87112@qq.com");
		helper.setSubject("主题：模板邮件");

		Map<String, Object> model = new HashedMap();
		model.put("username", "didi");
		String text = VelocityEngineUtils.mergeTemplateIntoString(
				velocityEngine, "template.vm", "UTF-8", model);
		helper.setText(text, true);

		mailSender.send(mimeMessage);
	}

}
