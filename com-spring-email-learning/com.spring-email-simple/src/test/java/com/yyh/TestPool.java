package com.yyh;

import com.yyh.component.AsyncTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Administrator on 2018/11/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TestPool {

    @Autowired
    AsyncTask asyncTask;





    @Test
    public void testPool(){
        for (int i = 0; i < 100; i++) {
            asyncTask.doTask(i);
        }
        System.out.println("结束测试。");
    }

    @Test
    public void testPool2(){
        for (int i = 0; i < 100; i++) {
            asyncTask.doTask2(i);
        }
        System.out.println("结束测试。");
    }
}
