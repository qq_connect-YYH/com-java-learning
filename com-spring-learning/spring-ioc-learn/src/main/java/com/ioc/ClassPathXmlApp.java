package com.ioc;

import com.ioc.service.MessageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 通过xml得到applicationContext
 * 学习：https://blog.csdn.net/nuomizhende45/article/details/81158383
 * Created by Administrator on 2019/8/19.
 */
public class ClassPathXmlApp {

    public static void main(String[] args) {
        //手动调试 配置文件的加载过程
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:application-xml.xml");

        MessageService bean = applicationContext.getBean(MessageService.class);
        String message = bean.getMessage();
        System.out.println(message);
    }
}
