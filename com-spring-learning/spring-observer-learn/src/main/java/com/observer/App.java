package com.observer;

import com.observer.event.DemoListener;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class App {

    public static void main(String[] args) {

        ConfigurableApplicationContext applicationContext = SpringApplication.run(App.class, args);
        ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
    }
}
