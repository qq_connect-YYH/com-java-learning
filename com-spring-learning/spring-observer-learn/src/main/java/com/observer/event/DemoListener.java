package com.observer.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 事件监听器角色
 */
@Component
public class DemoListener implements ApplicationListener<DemoEvent> {

    /**
     * Handle an application event.
     * @param event the event to respond to
     */
    public void onApplicationEvent(DemoEvent event) {
        //执行具体的业务
        String msg = event.getMessage();
        System.out.println("接收到的信息是："+msg);
    }
}
