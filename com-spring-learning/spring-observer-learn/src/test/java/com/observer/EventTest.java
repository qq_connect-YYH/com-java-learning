package com.observer;

import com.observer.event.DemoPublisher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventTest {

    @Autowired
    DemoPublisher demoPublisher;

    @Test
    public void test(){
        demoPublisher.publish("test message");
    }
}
