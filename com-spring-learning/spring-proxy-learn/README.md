#代理模式
[study](https://www.ibm.com/developerworks/cn/java/j-lo-proxy-pattern/index.html)
[study](https://github.com/code4wt/toy-spring)

1. 代理模式角色分为 4 种：        
主题接口：定义代理类和真实主题的公共对外方法，也是代理类代理真实主题的方法；      
真实主题：真正实现业务逻辑的类；        
代理类：用来代理和封装真实主题；        
Main：客户端，使用代理类和主题接口完成一些工作。      

2. 延迟加载     
- 代理模式实现延迟加载的方法及其意义。
用代理类封装真实类的调用，达到延迟加载。使用延迟加载可以提高应用的启动速度，并且可以节省资源开销。

- 手动实现的静态代理类延迟加载测试       
`com.proxy.manual_delay.Main`

3. 动态代理
- 动态代理是指在运行时动态生成代理类。

- 动态代理的好处？      
不会因为真实主体方法的改变而去改变代理。        
可以单独制定代理类的执行逻辑，提升系统的灵活性。      
  
- 生成动态代理类的方法？       
DK 自带的动态处理、CGLIB、Javassist 或者 ASM 库。

- jdk 动态代理 `com.proxy.dynamic_jdk.MainDynamic`