package com.proxy.dynamic_cglib;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainCglib {

    public static void main(String[] args) {
        BookProxyLib cglib = new BookProxyLib();

        BookProxyImpl bookCglib = (BookProxyImpl)cglib.getInstance(new BookProxyImpl());

        bookCglib.addBook();

        String str = "成都市(成华区)(武侯区)(高新区)";
        Pattern p = Pattern.compile(".*?(?=\\()");
        Matcher m = p.matcher(str);
        if(m.find()) {
            System.out.println(m.group());
        }
        boolean matches = str.matches(".*?(?=\\()");
        System.out.println(matches);

        System.out.println(-12 >> 3);
        System.out.println(-12 >>> 3);
    }
}
