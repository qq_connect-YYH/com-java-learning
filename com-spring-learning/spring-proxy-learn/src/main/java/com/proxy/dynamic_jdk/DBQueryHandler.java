package com.proxy.dynamic_jdk;

import cn.hutool.json.JSONUtil;
import com.proxy.manual_delay.DBQuery;
import com.proxy.manual_delay.IDBQuery;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 使用 JDK 的动态代理生成代理对象。JDK 的动态代理需要实现一个处理方法调用的 Handler，用于实现代理方法的内部逻辑。
 */
public class DBQueryHandler implements InvocationHandler {

    //定义主题接口
    IDBQuery realQuery = null;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //如果第一次调用，生成真实主题
        if(realQuery == null){
            realQuery = new DBQuery();
        }

        Object invoke = method.invoke(realQuery, args);

        //返回真实主题完成实际的操作
//        return realQuery.request();
        return invoke;
    }

    /**
     * 生成动态代理对象
     * @return
     */
    public static IDBQuery createProxy(){
        /*
        生成了一个实现了 IDBQuery 接口的代理类，代理类的内部逻辑由 DBQueryHandler 决定。
        生成代理类后，由 newProxyInstance() 方法返回该代理类的一个实例。一个完整的动态代理完成了。
         */
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        IDBQuery proxy = (IDBQuery) Proxy.newProxyInstance(
                systemClassLoader, new Class[]{IDBQuery.class}, new DBQueryHandler()
        );
        return proxy;
    }


}
