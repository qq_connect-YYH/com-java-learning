package com.proxy.dynamic_jdk;

import cn.hutool.json.JSONUtil;
import com.proxy.manual_delay.IDBQuery;

public class MainDynamic {

    public static void main(String[] args) {
        IDBQuery proxy = DBQueryHandler.createProxy();
        System.out.println("proxy" + JSONUtil.toJsonStr(proxy));

        String request = proxy.request();
        System.out.println(request);
    }
}
