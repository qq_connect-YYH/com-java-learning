package com.proxy.manual_delay;


/**
 * 真实主题：真正实现业务逻辑的类
 */
public class DBQuery implements IDBQuery {

    public DBQuery(){
        try{
            //假设数据库连接等耗时操作
            Thread.sleep(1000);
        }catch(InterruptedException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public String request() {
        return "request string";
    }


}
