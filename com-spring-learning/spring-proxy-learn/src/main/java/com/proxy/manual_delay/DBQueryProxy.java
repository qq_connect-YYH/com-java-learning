package com.proxy.manual_delay;


/**
 * 代理类：用来代理和封装真实主题；
 */
public class DBQueryProxy implements IDBQuery {

    private DBQuery real;

    @Override
    public String request() {
        //在真正需要的时候才能创建真实对象，创建过程可能很慢
        if(real == null){
            real = new DBQuery();
        }

        //在多线程环境下，这里返回一个虚假类，类似于 Future 模式
        return real.request();
    }


}
