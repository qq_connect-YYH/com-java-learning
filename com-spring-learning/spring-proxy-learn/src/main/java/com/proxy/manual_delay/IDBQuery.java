package com.proxy.manual_delay;

/**
 * 主题接口 定义对外的公共方法
 */
public interface IDBQuery {

    String request();

}
