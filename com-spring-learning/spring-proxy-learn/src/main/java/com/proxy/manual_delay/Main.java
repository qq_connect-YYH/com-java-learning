package com.proxy.manual_delay;


/**
 * 客户端，使用代理类和主题接口完成一些工作。
 */
public class Main {

    public static void main(String[] args){
        //使用代里
        IDBQuery q = new DBQueryProxy();

        //在真正使用时才创建真实对象
        String result = q.request();

        System.out.println(result);

    }
}
