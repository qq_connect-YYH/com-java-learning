# 日志相关学习
[参考DD](http://blog.didispace.com/spring-boot-learning-1x/)

1. Spring Boot中默认日志工具
- Spring Boot在所有内部日志中使用Commons Logging，但是默认配置也提供了对常用日志的支持，如：Java Util Logging，Log4J, Log4J2和Logback
- `com.logs.LogSimpleApplication`
```$xslt
根据不同的日志系统，可以按如下规则组织配置文件名:
Logback：logback-spring.xml, logback-spring.groovy, logback.xml, logback.groovy
Log4j：log4j-spring.properties, log4j-spring.xml, log4j.properties, log4j.xml
Log4j2：log4j2-spring.xml, log4j2.xml
JDK (Java Util Logging)：logging.properties
```

2. Spring boot中使用log4j记录日志
- `com.log4j.Log4jApplication`

3. Spring Boot中使用AOP统一处理Web请求日志
- 实现Web层的日志切面
```$xslt
实现AOP的切面主要有以下几个要素：

使用@Aspect注解将一个java类定义为切面类
使用@Pointcut定义一个切入点，可以是一个规则表达式，比如下例中某个package下的所有函数，也可以是一个注解等。
根据需要在切入点不同位置的切入内容：
    使用@Before在切入点开始处切入内容
    使用@After在切入点结尾处切入内容
    使用@AfterReturning在切入点return内容之后切入内容（可以用来对处理返回值做一些加工处理）
    使用@Around在切入点前后切入内容，并自己控制何时执行切入点自身的内容
    使用@AfterThrowing用来处理当切入内容部分抛出异常之后的处理逻辑
```
- AOP切面的优先级
```$xslt
由于通过AOP实现，程序得到了很好的解耦，但是也会带来一些问题，比如：我们可能会对Web层做多个切面，校验用户，校验头信息等等，这个时候经常会碰到切面的处理顺序问题。

所以，我们需要定义每个切面的优先级，我们需要@Order(i)注解来标识切面的优先级。i的值越小，优先级越高。假设我们还有一个切面是CheckNameAspect用来校验name必须为didi，我们为其设置@Order(10)，而上文中WebLogAspect设置为@Order(5)，所以WebLogAspect有更高的优先级，这个时候执行顺序是这样的：

在@Before中优先执行@Order(5)的内容，再执行@Order(10)的内容
在@After和@AfterReturning中优先执行@Order(10)的内容，再执行@Order(5)的内容
所以我们可以这样子总结：
    在切入点前的操作，按order的值由小到大执行
    在切入点后的操作，按order的值由大到小执行
```

- 动态切换日志级别
```$xslt
请求所有日志级别: get http://localhost:8899/loggers
切换日志级别: post http://localhost:8899/loggers/ROOT(这个依据返回的数据)
        { "configuredLevel": "INFO"}
        
```