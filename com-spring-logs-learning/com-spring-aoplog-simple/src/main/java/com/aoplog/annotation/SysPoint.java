package com.aoplog.annotation;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2019/6/21.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysPoint {

    /**
     * 描述
     * @return
     */
    String value() default "";

}
