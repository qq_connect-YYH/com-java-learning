package com.aoplog.conf;

import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HttpUtil;
import com.aoplog.annotation.SysPoint;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Administrator
 * @date 2019/6/21
 */
@Slf4j
@Aspect
@Component
public class WebLogAspect {

 /*   ThreadLocal<Long> startTime = new ThreadLocal<>();

    *//**
     * 使用@Pointcut定义一个切入点，可以是一个规则表达式，比如下例中某个package下的所有函数，也可以是一个注解等。
     *//*
    @Pointcut("execution(public * com.aoplog.controller..*.*(..))")
    public void webLog(){
        log.info("webLog");
    }

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        startTime.set(System.currentTimeMillis());

        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 记录下请求内容
        log.info("URL : " + request.getRequestURL().toString());
        log.info("HTTP_METHOD : " + request.getMethod());
        log.info("IP : " + request.getRemoteAddr());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));

    }

    @AfterReturning(returning = "ret", pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容
        log.info("RESPONSE : " + ret);
        log.info("SPEND TIME : " + (System.currentTimeMillis() - startTime.get()));
    }*/



    /************优化处理切面 取代上面的方式**************/

    /**
     * 基于表达式 这种不好 微服务系统 定义表达式不方便
     * 使用@Around在切入点前后切入内容，并自己控制何时执行切入点自身的内容
     *
     * @return
     */
//    @Around("execution(public * com.aoplog.controller..*.*(..))")
//    public Object aroundRegex(ProceedingJoinPoint point) throws Throwable {
//        String strClassName = point.getTarget().getClass().getName();
//        String strMethodName = point.getSignature().getName();
//        log.debug("[类名]:{},[方法]:{}", strClassName, strMethodName);
//
//        //1.执行切入点前的操作
//        Long startTime = System.currentTimeMillis();
//
//        //2.执行切入点自身的内容
//        Object obj = point.proceed();
//
//        //3.执行切入点后的操作 可以做成异步处理
//        Long endTime = System.currentTimeMillis();
//        log.info("SPEND TIME : " + (endTime - startTime));
//        return obj;
//    }

    /**
     * 基于注解
     * 使用@Around在切入点前后切入内容，并自己控制何时执行切入点自身的内容
     *
     * @return
     */
    @Around("@annotation(sysPoint)")
    public Object aroundAnnotation(ProceedingJoinPoint point, SysPoint sysPoint) throws Throwable {
        String strClassName = point.getTarget().getClass().getName();
        String strMethodName = point.getSignature().getName();
        log.debug("[类名]:{},[方法]:{}", strClassName, strMethodName);

        //1.执行切入点前的操作
        Long startTime = System.currentTimeMillis();
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        log.info("Ip "+ HttpUtil.getClientIP(request));
        log.info("URI "+ URLUtil.getPath(request.getRequestURI()));
        log.info("Params "+ URLUtil.getPath(HttpUtil.toParams(request.getParameterMap())));
        log.info("Agent "+ request.getHeader("user-agent"));
        log.info("Method "+ request.getMethod());


        //2.执行切入点自身的内容
        Object obj = point.proceed();

        //3.执行切入点后的操作 可以做成异步处理
        Long endTime = System.currentTimeMillis();
        log.info("SPEND TIME : " + (endTime - startTime));
        return obj;
    }

}
