package com.aoplog.controller;

import com.aoplog.annotation.SysPoint;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Administrator
 * @date 2019/6/21
 */
@RestController
public class AopController {

    @RequestMapping(value = "/aop", method = RequestMethod.GET)
    public String aop(@RequestParam String name) {
        return "aop " + name;
    }

    @SysPoint("SysPoint")
    @RequestMapping(value = "/syspoint", method = RequestMethod.GET)
    public String SysPoint(@RequestParam String name) {
        return "SysPoint aop " + name;
    }
}
