package com.aoplog.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/6/21.
 */
@Slf4j
@RestController
public class LogController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String testLogLevel() {
        log.debug("Logger Level ：DEBUG");
        log.info("Logger Level ：INFO");
        log.error("Logger Level ：ERROR");
        return "";
    }
}
