package com.log4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2019/6/21.
 */
@SpringBootApplication
public class Log4jApplication {

    public static void main(String[] args) {
        SpringApplication.run(Log4jApplication.class, args);
    }
}
