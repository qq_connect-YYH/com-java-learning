package com.log4j.entity;

import lombok.Data;

/**
 * Created by Administrator on 2019/6/19.
 */
@Data
public class User {

    private Long id;
    private String name;
    private Integer age;
    private String addr;
}
