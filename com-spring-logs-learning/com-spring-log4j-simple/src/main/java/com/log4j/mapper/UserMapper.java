package com.log4j.mapper;

import com.log4j.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Administrator on 2019/6/19.
 */
@Mapper
public interface UserMapper {

    @Select("SELECT * FROM USER WHERE NAME = #{name}")
    List<User> findByName(@Param("name") String name);

    @Select("SELECT * FROM USER WHERE ID = ${id}")
    User findById(@Param("id") Long id);

    @Select("SELECT name, age, addr FROM USER")
    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "age", column = "age")
    })
    List<User> findAll();


    @Insert("INSERT INTO USER(NAME, AGE, ADDR) VALUES(#{name}, #{age}, #{addr})")
    int insert(User user);

    @Update("UPDATE USER SET NAME=#{name}, AGE=#{age}, ADDR=#{addr} WHERE ID =  ${id}")
    int updateById(User user);

    @Delete("DELETE FROM USER WHERE ID = ${id}")
    int deleteById(@Param("id") Long id);

}
