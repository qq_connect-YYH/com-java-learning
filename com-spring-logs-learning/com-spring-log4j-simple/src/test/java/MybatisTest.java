import com.alibaba.fastjson.JSONObject;
import com.log4j.Log4jApplication;
import com.log4j.entity.User;
import com.log4j.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfigurationAttributes;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by Administrator on 2019/6/19.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Log4jApplication.class)
public class MybatisTest {

    @Autowired
    UserMapper userMapper;


    @Test
//    @Transactional
    public void testHuanDu2(){
        List<User> all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));

        //打个断点  看cacheManager 缓存了什么
        all = userMapper.findAll();
        System.out.println(JSONObject.toJSONString(all));
    }
}
