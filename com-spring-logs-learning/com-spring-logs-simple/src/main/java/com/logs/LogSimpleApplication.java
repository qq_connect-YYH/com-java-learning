package com.logs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2019/6/21.
 */
@SpringBootApplication
public class LogSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogSimpleApplication.class, args);
    }
}
