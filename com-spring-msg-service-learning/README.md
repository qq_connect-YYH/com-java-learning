### 消息服务学习
[参考DD](http://blog.didispace.com/spring-boot-learning-1x/)

1. springBoot 整合 RabbitMQ 学习
- 安装Erland
```$xslt
a.下载地址: https://www.rabbitmq.com/download.html 
b.直接安装
c.设置path: %ERLANG_HOME%\bin
```
- 安装RabbitMQ
```$xslt
a.下载地址： https://www.rabbitmq.com/download.html
b. 直接安装，要先安装好erland
c. 设置path： %RABBITMQ_SERVER%\sbin
d. 管理员运行cmd并执行： rabbitmq-plugins.bat enable rabbitmq_management 或 rabbitmq-plugins enable rabbitmq_management
e. 访问：http://localhost:15672/  账号：guest 密码：guest

```

