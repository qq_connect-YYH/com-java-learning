package com.rabbitmq.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息消费者
 * @author Administrator
 * @date 2019/6/21
 */
@Slf4j
@Component
@RabbitListener(queues = {"hello"})
public class Receiver {

    @RabbitHandler
    public void process(String hello){
        log.info(String.format("接收到的消息：%s ", hello));
    }

}
