package com.rabbitmq.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 消息生产者
 *
 * @author Administrator
 * @date 2019/6/21
 */
@Slf4j
@Component
public class Sender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void send(){
        //会产生一个字符串，并发送到名为hello的队列中
        String context = String.format("hello ", new Date());
        log.info(String.format("Sender : %s", context));
        amqpTemplate.convertAndSend("hello" ,context);

        rabbitTemplate.convertAndSend("hello", context);
    }
}
