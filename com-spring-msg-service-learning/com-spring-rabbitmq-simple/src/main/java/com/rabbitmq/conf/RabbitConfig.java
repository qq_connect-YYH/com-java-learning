package com.rabbitmq.conf;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 创建RabbitMQ的配置类RabbitConfig，用来配置队列、交换器、路由等高级信息。
 * @author Administrator
 * @date 2019/6/21
 */
@Configuration
public class RabbitConfig {


    @Bean
    public Queue helloQueue(){
        return new Queue("hello");
    }

}
