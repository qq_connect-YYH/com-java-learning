import com.rabbitmq.RabbitMQApplication;
import com.rabbitmq.component.Sender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/21.
 */
@SpringBootTest(classes = RabbitMQApplication.class)
@RunWith(SpringRunner.class)
public class RabbitTest {

    @Autowired
    private Sender sender;


    @Test
    public void hello(){
        sender.send();
    }
}
