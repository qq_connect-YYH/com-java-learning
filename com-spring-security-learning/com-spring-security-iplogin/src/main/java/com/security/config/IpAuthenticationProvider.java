package com.security.config;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 2019/4/5.
 */
public class IpAuthenticationProvider implements AuthenticationProvider {

    final static Map<String, SimpleGrantedAuthority> ipAuthorityMap = new ConcurrentHashMap();
    //维护ip白名单列表
    static {
       ipAuthorityMap.put("127.0.0.1", new SimpleGrantedAuthority("ADMIN"));
       ipAuthorityMap.put("192.168.152.1", new SimpleGrantedAuthority("ADMIN"));
       ipAuthorityMap.put("192.168.15.0", new SimpleGrantedAuthority("FRIEND"));
    }
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        IpAuthenticationToken token = (IpAuthenticationToken) authentication;
        String ip = token.getIp();
        SimpleGrantedAuthority result = ipAuthorityMap.get(ip);
        if(result != null){
            //封裝权限信息，并且此时身份已经被认证
            return new IpAuthenticationToken(ip, Arrays.asList(result));
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return false;
    }
}
