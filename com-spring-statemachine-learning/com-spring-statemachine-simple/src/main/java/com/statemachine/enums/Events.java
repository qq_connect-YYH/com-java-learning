package com.statemachine.enums;

/**
 * Created by Administrator on 2019/6/23.
 *
 * 支付事件PAY会触发状态从待支付UNPAID状态到待收货WAITING_FOR_RECEIVE状态的迁移，
 * 而收货事件RECEIVE会触发状态从待收货WAITING_FOR_RECEIVE状态到结束DONE状态的迁移。
 */
public enum Events {
    PAY,        //支付
    RECEIVE,    //收费
}
