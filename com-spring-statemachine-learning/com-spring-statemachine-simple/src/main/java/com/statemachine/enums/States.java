package com.statemachine.enums;

/**
 * Created by Administrator on 2019/6/23.
 */
public enum States {
    UNPAID,                 // 待支付
    WAITING_FOR_RECEIVE,    // 待收货
    DONE,                   // 结束
}
