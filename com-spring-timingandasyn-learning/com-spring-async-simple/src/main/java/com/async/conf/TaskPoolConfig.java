package com.async.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *  自定义线程池的配置
 * Created by Administrator on 2019/6/23.
 */
@Configuration
public class TaskPoolConfig {

    /**
     * 这里可以写个配置文件 解析
     * @return
     */
    @Bean("customExecutor")
    public Executor customExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程数。线程池创建时候初始化的线程数
        executor.setCorePoolSize(3);
        //最大线程数。线程池最大的线程数，只有在缓冲队列满了之后才会申请超过核心线程数的线程
        executor.setMaxPoolSize(20);
        //缓存队列。用来缓冲执行任务的队列
        executor.setQueueCapacity(100);
        //允许线程的空闲时间。当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
        executor.setKeepAliveSeconds(60);
        //线程池名的前缀。设置好了之后可以方便我们定位处理任务所在的线程池
        executor.setThreadNamePrefix("customExecutor-");
        //线程池对拒绝任务的处理策略。
        //这里采用了CallerRunsPolicy策略，当线程池没有处理能力的时候，该策略会直接在 execute 方法的调用线程中运行被拒绝的任务；如果执行程序已关闭，则会丢弃该任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        //用来设置线程池关闭的时候等待所有任务都完成再继续销毁其他的Bean，这样这些异步任务的销毁就会先于Redis线程池的销毁。
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;

        /***
         * Spring ThreadPoolTaskExecutor没有使用阻塞模式将任务加入到对象中，因此对象满的时候会抛出异常，对于这种情况，一般的企业执行环境不能无限制的增大内存队列容量，因此不得不阻塞队列的加入，spring内置提供的异常处理机制不好用，因为ThreadPoolExecutor.CallerRunsPolicy的处理方式是将异常任务放在调用线程中执行，这样对于单个执行时间长的任务，即使队列有空闲了，剩下的任务也要等这个任务在主线程执行完了才能继续往队列里面添加。有一个处理方法就是捕获executor.execute()的异常，只要发现有异常就等待一段时间，直到没有异常为止，这样就能模仿阻塞队列的效果，下面是代码：
         while(true){
             try{
                 taskExecutor.execute(new MailSender(tUserIssueInfo));
                 break;
             }catch(TaskRejectedException e){
                 try{
                    Thread.sleep(1000);
                 }catch(Exception e2){}
             }
         }
         */
    }
}
