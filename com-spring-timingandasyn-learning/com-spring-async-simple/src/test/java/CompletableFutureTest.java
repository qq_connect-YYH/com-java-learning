import com.async.AsyncApplication;
import com.async.componet.TasksSync;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;
import java.util.concurrent.*;
import java.util.function.*;

/**
 * Created by Administrator on 2019/6/22.
 * 参考 https://www.jianshu.com/p/6bac52527ca4
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AsyncApplication.class})
public class CompletableFutureTest {
    @Autowired
    private TasksSync tasksSync;

    /************ 1. runAsync 和 supplyAsync方法 **************/
    @Test
    public void runAsync() throws ExecutionException, InterruptedException {
        //无返回值异步
        long start = System.currentTimeMillis();
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                tasksSync.doTaskOne();
            } catch (Exception e) {
                e.printStackTrace();
            }
            log.info("run end ...");
        });
        long end = System.currentTimeMillis();
        future.get();
        log.info("执行时间: "+ (end - start) + " 毫秒。");
    }

    @Test
    public void supplierAsync() throws ExecutionException, InterruptedException {
        //有返回值异步
        long start = System.currentTimeMillis();
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
            }
            System.out.println("run end ...");
            return "测试结果";
        });
        String s = future.get();
        long end = System.currentTimeMillis();
        log.info("执行结果: "+s+"  执行时间: "+ (end - start) + " 毫秒。");
    }


    /*************** 2. 计算结果完成时的回调方法
     public CompletableFuture<T> whenComplete(BiConsumer<? super T,? super Throwable> action)
     public CompletableFuture<T> whenCompleteAsync(BiConsumer<? super T,? super Throwable> action)
     public CompletableFuture<T> whenCompleteAsync(BiConsumer<? super T,? super Throwable> action, Executor executor)
     public CompletableFuture<T> exceptionally(Function<Throwable,? extends T> fn)
     *
     whenComplete 和 whenCompleteAsync 的区别：
     whenComplete：是执行当前任务的线程执行继续执行 whenComplete 的任务。
     whenCompleteAsync：是执行把 whenCompleteAsync 这个任务继续提交给线程池来进行执行。
     * ****************/

    @Test
    public void testResult() throws ExecutionException, InterruptedException {
        //这种方式  异常进入到 exceptionally 里面！
        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (new Random().nextInt() % 2 >= 0) {
                int i = 1 / 0;
            }
            log.info("run end ...");
            return 1L;
        });
        //计算结果
        CompletableFuture<Long> futrue2 = future.whenComplete(new BiConsumer<Long, Throwable>() {
            @Override
            public void accept(Long aLong, Throwable throwable) {
                log.info(String.format("返回的结果：%s ", aLong));
                aLong += aLong;
            }
        });
        log.info(String.format("futrue2返回 %s", futrue2.get()));

        //异常处理
        CompletableFuture<Long> exceptionally = future.exceptionally(new Function<Throwable, Long>() {
            @Override
            public Long apply(Throwable throwable) {
                log.info(String.format("异常信息：%s ", throwable.getMessage()));
                return 2L;
            }
        });
        log.info(String.format("exceptionally返回 %s", futrue2.get()));
        TimeUnit.SECONDS.sleep(2);
    }

    @Test
    public void testResult2() throws ExecutionException, InterruptedException {
        //这种方式异常可以进入到 exceptionally
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (new Random().nextInt() % 2 >= 0) {
                int i = 1 / 0;
            }
            log.info("run end ...");
        });
        //计算结果
        future.whenComplete(new BiConsumer<Void, Throwable>() {
            @Override
            public void accept(Void aVoid, Throwable throwable) {
                log.info("ok");
            }
        });

        //异常处理
        future.exceptionally(new Function<Throwable, Void>() {
            @Override
            public Void apply(Throwable throwable) {
                log.info("异常");
                return null;
            }
        }) ;
        TimeUnit.SECONDS.sleep(2);
    }


    /************* 3. thenApply 方法 ****************
     * 当一个线程依赖另一个线程时，可以使用 thenApply 方法来把这两个线程串行化。
     *
     public <U> CompletableFuture<U> thenApply(Function<? super T,? extends U> fn)      //这个回到主线程执行
     public <U> CompletableFuture<U> thenApplyAsync(Function<? super T,? extends U> fn) //这个在上一步的异步线程里执行
     public <U> CompletableFuture<U> thenApplyAsync(Function<? super T,? extends U> fn, Executor executor)

     Function<? super T,? extends U>
     T：上一个任务返回结果的类型
     U：当前任务的返回值类型
     */

    @Test
    public void thenApply() throws ExecutionException, InterruptedException {
        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            long val = new Random().nextInt(1000);
            log.info("随机值 " + val);
            return val;
        })
//        .thenApply(new Function<Long, Long>() {
        .thenApplyAsync(new Function<Long, Long>() {
            @Override
            public Long apply(Long aLong) {
                aLong = aLong * 5;
                log.info("接收值 " + aLong);
                return aLong;
            }
        });
        log.info("计算值 "+ future.get());

        //第二个任务依赖第一个任务的结果。
    }


    /***************** 4. handle 方法 ************************
     handle 是执行任务完成时对结果的处理。
     handle 方法和 thenApply 方法处理方式基本一样。
     不同的是 handle 是在任务完成后再执行，还可以处理异常的任务。
     thenApply 只可以执行正常的任务，任务出现异常则不执行 thenApply 方法。

     public <U> CompletionStage<U> handle(BiFunction<? super T, Throwable, ? extends U> fn);
     public <U> CompletionStage<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn);
     public <U> CompletionStage<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn,Executor executor);
     */

    @Test
    public void handle() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            int i = 10 / 0;
            return new Random().nextInt(1000);
        })
//        .handle(new BiFunction<Integer, Throwable, Integer>() {
        .handleAsync(new BiFunction<Integer, Throwable, Integer>() {
            @Override
            public Integer apply(Integer integer, Throwable throwable) {
                int result = -1;
                if(throwable == null){
                    result = integer * 6;
                }else{
                    log.info("执行异常 "+ throwable.getMessage());
                }
                return result;
            }
        });
        log.info("结果 "+ future.get());

        //从示例中可以看出，在 handle 中可以根据任务是否有异常来进行做相应的后续处理操作。
        // 而 thenApply 方法，如果上个任务出现错误，则不会执行 thenApply 方法。
    }


    /*************** 5. thenAccept 消费处理结果 **************
     *  接收任务的处理结果，并消费处理，无返回结果。
     public CompletionStage<Void> thenAccept(Consumer<? super T> action);
     public CompletionStage<Void> thenAcceptAsync(Consumer<? super T> action);
     public CompletionStage<Void> thenAcceptAsync(Consumer<? super T> action,Executor executor);
     */

    @Test
    public void thenAccept() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            return new Random().nextInt(1000);
        }).thenAccept(val -> {
            log.info("接受的值 " + val);
        });

        log.info("结果 "+ future.get());
        //从示例代码中可以看出，该方法只是消费执行完成的任务，并可以根据上面的任务返回的结果进行处理。并没有后续的输错操作。
    }


    /************* 6. thenRun 方法 *****************
     * 跟 thenAccept 方法不一样的是，不关心任务的处理结果。只要上面的任务执行完成，就开始执行 thenRun 。
     public CompletionStage<Void> thenRun(Runnable action);
     public CompletionStage<Void> thenRunAsync(Runnable action);
     public CompletionStage<Void> thenRunAsync(Runnable action,Executor executor);
     */
    @Test
    public void thenRun() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            int nextInt = new Random().nextInt(100);
            log.info("计算值 " + nextInt);
            return nextInt;
        }).thenRunAsync(() -> {
            log.info("开始执行自己的任务了 ");
        });
        log.info("结果 "+ future.get());

        //该方法同 thenAccept 方法类似。
        // 不同的是上个任务处理完成后，并不会把计算的结果传给 thenRun 方法。只是处理完任务后，执行 thenRun 的后续操作。
    }

    /************ 7. thenCombine 合并任务 *************
     * thenCombine 会把 两个 CompletionStage 的任务都执行完成后，把两个任务的结果一块交给 thenCombine 来处理。
     public <U,V> CompletionStage<V> thenCombine(CompletionStage<? extends U> other,BiFunction<? super T,? super U,? extends V> fn);
     public <U,V> CompletionStage<V> thenCombineAsync(CompletionStage<? extends U> other,BiFunction<? super T,? super U,? extends V> fn);
     public <U,V> CompletionStage<V> thenCombineAsync(CompletionStage<? extends U> other,BiFunction<? super T,? super U,? extends V> fn,Executor executor);
     */

    @Test
    public void thenCombine() throws ExecutionException, InterruptedException {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            log.info("future1");
            return "hello yi";
        });
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            log.info("future2");
            return "hello chen";
        });

        CompletableFuture<String> result = future1.thenCombineAsync(future2, new BiFunction<String, String, String>() {
            @Override
            public String apply(String t, String u) {
                log.info("thenCombine");
                return t + " " + u;
            }
        });
        log.info("结果 "+ result.get());
    }

    /************ 8. thenAcceptBoth************
     * 当两个CompletionStage都执行完成后，把结果一块交给thenAcceptBoth来进行消耗
     public <U> CompletionStage<Void> thenAcceptBoth(CompletionStage<? extends U> other,BiConsumer<? super T, ? super U> action);
     public <U> CompletionStage<Void> thenAcceptBothAsync(CompletionStage<? extends U> other,BiConsumer<? super T, ? super U> action);
     public <U> CompletionStage<Void> thenAcceptBothAsync(CompletionStage<? extends U> other,BiConsumer<? super T, ? super U> action,     Executor executor);
     */
    @Test
    public void thenAcceptBoth(){

        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("f1="+t);
                return t;
            }
        });

        CompletableFuture<Integer> f2 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("f2="+t);
                return t;
            }
        });
        f1.thenAcceptBothAsync(f2, new BiConsumer<Integer, Integer>() {
            @Override
            public void accept(Integer t, Integer u) {
              log.info("f1="+t+";f2="+u+";");
            }
        });
        // TODO: 2019/6/22 注意无限循环
        while (true){}
    }

    /*********************** 9. applyToEither 方法
     * 两个CompletionStage，谁执行返回的结果快，我就用那个CompletionStage的结果进行下一步的转化操作。
     public <U> CompletionStage<U> applyToEither(CompletionStage<? extends T> other,Function<? super T, U> fn);
     public <U> CompletionStage<U> applyToEitherAsync(CompletionStage<? extends T> other,Function<? super T, U> fn);
     public <U> CompletionStage<U> applyToEitherAsync(CompletionStage<? extends T> other,Function<? super T, U> fn,Executor executor);
     */
    @Test
    public void applyToEither() throws ExecutionException, InterruptedException {
        // TODO: 2019/6/22 有个问题 睡眠时间一长  程序就卡在那里了
        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("f1="+t);
                return t;
            }
        });
        CompletableFuture<Integer> f2 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
               log.info("f2="+t);
                return t;
            }
        });

        CompletableFuture<Integer> result = f1.applyToEither(f2, new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer t) {
                log.info("这个是谁的值 " + t);
                return t * 2;
            }
        });

       log.info("结果 "+ result.get());
        while (true){}
    }

    /********* 10. acceptEither 方法 ***********
     * 两个CompletionStage，谁执行返回的结果快，我就用那个CompletionStage的结果进行下一步的消耗操作。
     public CompletionStage<Void> acceptEither(CompletionStage<? extends T> other,Consumer<? super T> action);
     public CompletionStage<Void> acceptEitherAsync(CompletionStage<? extends T> other,Consumer<? super T> action);
     public CompletionStage<Void> acceptEitherAsync(CompletionStage<? extends T> other,Consumer<? super T> action,Executor executor);
     */
    @Test
    public void acceptEither() throws ExecutionException, InterruptedException {
        // TODO: 2019/6/22 感觉只要sleep 大于0 ，2个supplyAsync 都不会有结果了
        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
//                    TimeUnit.SECONDS.sleep(1);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("f1="+t);
                return t;
            }
        });

        CompletableFuture<Integer> f2 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
//                    TimeUnit.SECONDS.sleep(1);
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
               log.info("f2="+t);
                return t;
            }
        });
        f1.acceptEitherAsync(f2, new Consumer<Integer>() {
            @Override
            public void accept(Integer t) {
                log.info("这是哪个的值 "+ t);
            }
        });

        //理论上  我们服务的主线程是不会关闭的，这是测试，测试主程序结束所以会导致这个问题吧。
        while (true){}
    }

    /*********1 1. runAfterEither 方法***********
     两个CompletionStage，任何一个完成了都会执行下一步的操作（Runnable）
     public CompletionStage<Void> runAfterEither(CompletionStage<?> other,Runnable action);
     public CompletionStage<Void> runAfterEitherAsync(CompletionStage<?> other,Runnable action);
     public CompletionStage<Void> runAfterEitherAsync(CompletionStage<?> other,Runnable action,Executor executor);
     */
    @Test
    public void runAfterEither(){
        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("f1="+t);
                return t;
            }
        });
        CompletableFuture<Integer> f2 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("f2="+t);
                return t;
            }
        });
        f1.runAfterEither(f2, new Runnable() {
            @Override
            public void run() {
               log.info("上面有一个已经完成了。");
            }
        });
        while (true){}
    }

    /******* 12. runAfterBoth *******
     * 两个CompletionStage，都完成了计算才会执行下一步的操作（Runnable）
     public CompletionStage<Void> runAfterBoth(CompletionStage<?> other,Runnable action);
     public CompletionStage<Void> runAfterBothAsync(CompletionStage<?> other,Runnable action);
     public CompletionStage<Void> runAfterBothAsync(CompletionStage<?> other,Runnable action,Executor executor);
     */
    @Test
    public void runAfterBoth(){
        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("f1="+t);
                return t;
            }
        });
        CompletableFuture<Integer> f2 = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                try {
                    TimeUnit.SECONDS.sleep(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("f2="+t);
                return t;
            }
        });
        f1.runAfterBoth(f2, () ->{
            log.info("上面两个任务都执行完成了。");
        });

        //这是测试类 所以得加速这个
        while (true){
        }
    }

    /********** 13. thenCompose 方法************
     * thenCompose 方法允许你对两个 CompletionStage 进行流水线操作，第一个操作完成时，将其结果作为参数传递给第二个操作。
     */
    @Test
    public void thenCompose() throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> f = CompletableFuture.supplyAsync(new Supplier<Integer>() {
            @Override
            public Integer get() {
                int t = new Random().nextInt(3);
                log.info("t1="+t);
                return t;
            }
        })
//       .thenCompose(new Function<Integer, CompletionStage<Integer>>() {
         .thenComposeAsync(new Function<Integer, CompletionStage<Integer>>() {
            @Override
            public CompletionStage<Integer> apply(Integer param) {
                return CompletableFuture.supplyAsync(new Supplier<Integer>() {
                    @Override
                    public Integer get() {
                        int t = param *2;
                        log.info("t2="+t);
                        return t;
                    }
                });
            }
        });
       log.info("thenCompose result : "+f.get());
       while (true){}
    }
}
