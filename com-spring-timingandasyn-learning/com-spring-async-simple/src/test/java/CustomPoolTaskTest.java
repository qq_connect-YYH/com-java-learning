import com.async.AsyncApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

/**
 * 自定义线程池  异步调用
 * Created by Administrator on 2019/6/23.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AsyncApplication.class)
public class CustomPoolTaskTest {

    //表明哪个实现类才是我们所需要的,注入自定义线程池
    @Qualifier("customExecutor")
    @Autowired
    private Executor executor;

    @Test
    public void customPoolTest() throws ExecutionException, InterruptedException {
        //带返回值
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> {
            int val = new Random().nextInt(1000);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            int n = 0;
//            while (n < 100000000){
//                n++;
//            }
            log.info("future2 当前线程执行的值 " + val);
            return val;
        }, executor);
        log.info("结果 "+ future2.get());
    }

    @Test
    public void testMaxPool() throws Exception {
        //不带返回值 测试最大线程数阻塞
        // 阻塞任务没执行完就 会报错 java.lang.InterruptedException: sleep interrupted 【感觉又是主线程结束导致的】
        for (int i = 0; i < 100; i++){
            Thread.sleep(200);
            final int var = i;
            CompletableFuture.runAsync(() -> {
                int val = new Random().nextInt(10000);
                log.info("future:"+ var + "  当前线程执行的值 " + val);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }, executor);
        }
        log.info("结束");
        while (true){}
    }
}
