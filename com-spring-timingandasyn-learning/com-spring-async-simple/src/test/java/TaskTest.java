import com.async.AsyncApplication;
import com.async.componet.TasksAsync;
import com.async.componet.TasksAsyncReturn;
import com.async.componet.TasksSync;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Future;

/**
 * Created by Administrator on 2019/6/22.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AsyncApplication.class})
public class TaskTest {

    @Autowired
    private TasksSync tasksSync;
    @Autowired
    private TasksAsync tasksAsync;
    @Autowired
    private TasksAsyncReturn tasksAsyncReturn;

    /**
     * 同步调用测试
     * @throws Exception
     */
    @Test
    public void testSync() throws Exception {
        tasksSync.doTaskOne();
        tasksSync.doTaskTwo();
        tasksSync.doTaskThree();
        //结论：oTaskOne、doTaskTwo、doTaskThree三个函数顺序的执行完成。
    }


    /**
     * 异步调用
     * @throws Exception
     */
    @Test
    public void testAsync() throws Exception {
        tasksAsync.doTaskOne();
        tasksAsync.doTaskTwo();
        tasksAsync.doTaskThree();
        //1.做个无线循环，为了等待异步线程执行结束
//        while (true){
//            Thread.sleep(10000);
//            System.out.println("等待中...");
//        }
        //2. 阻塞main线程直至main线程执行one，tow，three 方法后结束
        Thread.currentThread().join();


        //结论：异步线程执行顺序是随机的，无法掌控。
        //@Async所修饰的函数不要定义为static类型，这样异步调用不会生效
    }

    /**
     * 异步调用回调结果
     * @throws Exception
     */
    @Test
    public void testAsyncReturn() throws Exception {
        long start = System.currentTimeMillis();
        Future<String> taskOne = tasksAsyncReturn.doTaskOne();
        Future<String> taskTwo = tasksAsyncReturn.doTaskTwo();
        Future<String> doTaskThree = tasksAsyncReturn.doTaskThree();

        //使用Future 判断异步线程是否执行完成
        while (true){
            if(taskOne.isDone() && taskTwo.isDone() && doTaskThree.isDone()){
                break;
            }
            Thread.sleep(1000);
            System.out.println("等待中...");
        }
        log.info("结果： "+ taskOne.get());
        long end = System.currentTimeMillis();
        log.info("执行时间: "+ (end - start) + " 毫秒。");

        // 如何使用：CompletableFuture
    }


}
