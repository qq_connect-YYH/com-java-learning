package com.scheduled.componet;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2019/6/22.
 */
@Slf4j
@Component
public class ScheduledTasks {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:sss");

    /**
     * 每5秒执行一次
     */
//    @Scheduled(fixedDelay = 5000)
    public void task(){
        log.info("现在时刻："+ dateFormat.format(new Date()));
    }

    /**
     * 第一次延迟2s执行，之后每6s执行一次
     */
//    @Scheduled(initialDelay = 2000, fixedDelay = 6000)
    public void task2(){
        log.info("现在时刻："+ dateFormat.format(new Date()));
    }

    /**
     * cron 表达式
     *
     */
    @Scheduled(cron = "*/5 * * * * *")
    public void taskCan(){
        log.info("现在时刻："+ dateFormat.format(new Date()));
    }
}
