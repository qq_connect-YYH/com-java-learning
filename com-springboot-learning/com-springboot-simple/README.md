### 本项目记录的问题
1. 静态资源访问
Spring Boot默认提供静态资源目录位置需置于classpath下，目录名需符合如下规则：
/static
/public
/resources
/META-INF/resources

2. 模板引擎
在动态HTML实现上Spring Boot依然可以完美胜任，并且提供了多种模板引擎的默认配置支持，所以在推荐的模板引擎下，我们可以很快的上手开发动态网站。
Spring Boot提供了默认配置的模板引擎主要有以下几种：     
Thymeleaf       
FreeMarker      
Velocity        
Groovy      
Mustache        
Spring Boot建议使用这些模板引擎，避免使用JSP，若一定要使用JSP将无法实现Spring Boot的多种特性，具体可见后文：支持JSP的配置        
当你使用上述模板引擎中的任何一个，它们默认的模板配置路径为：src/main/resources/templates。当然也可以修改这个路径，具体如何修改，可在后续各模板引擎的配置属性中查询并修改。     

- Thymeleaf的默认参数配置
```
# Enable template caching.
spring.thymeleaf.cache=true 
# Check that the templates location exists.
spring.thymeleaf.check-template-location=true 
# Content-Type value.
spring.thymeleaf.content-type=text/html 
# Enable MVC Thymeleaf view resolution.
spring.thymeleaf.enabled=true 
# Template encoding.
spring.thymeleaf.encoding=UTF-8 
# Comma-separated list of view names that should be excluded from resolution.
spring.thymeleaf.excluded-view-names= 
# Template mode to be applied to templates. See also StandardTemplateModeHandlers.
spring.thymeleaf.mode=HTML5 
# Prefix that gets prepended to view names when building a URL.
spring.thymeleaf.prefix=classpath:/templates/ 
# Suffix that gets appended to view names when building a URL.
spring.thymeleaf.suffix=.html  
spring.thymeleaf.template-resolver-order= 
# Order of the template resolver in the chain. 
spring.thymeleaf.view-names= 
# Comma-separated list of view names that can be resolved.
```

3. 使用Swagger2构建RESTful API文档
- RESTful API的重磅好伙伴Swagger2，它可以轻松的整合到Spring Boot中，并与Spring MVC程序配合组织出强大RESTful API文档。       
它既可以减少我们创建文档的工作量，同时说明内容又整合入实现代码中，让维护文档和修改代码整合为一体，可以让我们在修改代码逻辑的同时方便的修改文档说明。      
另外Swagger2也提供了强大的页面测试功能来调试每个RESTful API。        
- com.example.demo.Swagger2

4. 统一异常处理
- Spring Boot提供了一个默认的映射：/error，当处理中抛出异常之后，会转到该请求中处理，并且该请求有一个全局的错误页面用来展示异常内容。
- com.example.demo.config.GlobalExceptionHandler
- com.example.demo.controller.GlobalExceptionController

5. Spring Boot和Feign中使用Java 8时间日期API（LocalDate等）的序列化问题
- [参照DD](http://blog.didispace.com/Spring-Boot-And-Feign-Use-localdate/)

6. Spring Boot中如何扩展XML请求和响应的支持
- 引入Xml消息转换器
- 定义对象与Xml的关系
`com.example.demo.xmlvo.UserXml`
- 创建接收xml请求的接口
`com.example.demo.controller.UserXmlController`
```
<UserXml>
	<name>test user</name>
	<age>17</age>
</UserXml>
```
- Accept 控制返回结果 application/json 或 application/xml
