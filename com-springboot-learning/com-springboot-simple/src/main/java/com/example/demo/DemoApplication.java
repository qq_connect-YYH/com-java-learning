package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication();
		//禁用解析命令行参数
		springApplication.setAddCommandLineProperties(false);
		springApplication.run(DemoApplication.class, args);
	}

}
