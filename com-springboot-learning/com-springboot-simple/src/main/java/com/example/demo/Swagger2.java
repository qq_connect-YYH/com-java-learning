package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author Administrator
 * @date 2019/6/15
 * 访问：http://ip:port/swagger-ui.html
 */
//Spring来加载该类配置
@Configuration
//启用Swagger2
@EnableSwagger2
public class Swagger2 {

    @Bean
    public Docket createRestApi(){
        Docket bean = new Docket(DocumentationType.SWAGGER_2)
                //创建该Api的基本信息
                .apiInfo(apiInfo())
                //控制哪些接口暴露给Swagger来展现
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo"))
                .paths(PathSelectors.any())
                .build();
        return bean;
    }

    private ApiInfo apiInfo(){
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Spring Boot中使用Swagger2构建RESTful APIs")
                .description("更多Spring Boot相关文章请关注：http://blog.yyh.com/")
                .termsOfServiceUrl("http://blog.yyh.com/")
                .contact("YYH")
                .version("1.0")
                .build();
        return apiInfo;
    }
}
