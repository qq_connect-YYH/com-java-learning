package com.example.demo.componet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2019/6/11.
 */
@Component
public class PropertiesConfig {

    @Value("${com.yyh.name}")
    private String name;
    @Value("${com.yyh.title}")
    private String title;
    @Value("${com.didispace.blog.value}")
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
