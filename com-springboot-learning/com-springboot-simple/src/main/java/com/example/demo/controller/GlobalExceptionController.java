package com.example.demo.controller;

import com.example.demo.exception.CustomDataException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 * @date 2019/6/15
 */
@RestController
public class GlobalExceptionController {

    @GetMapping("/testError")
    public String testError() throws Exception {
        throw new Exception("测试异常");
    }

    @GetMapping("/customException")
    public String customException() {
        throw new CustomDataException("自定义测试异常，返回json格式。");
    }


}
