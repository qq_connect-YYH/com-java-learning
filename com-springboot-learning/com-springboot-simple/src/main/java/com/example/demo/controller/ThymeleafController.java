package com.example.demo.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2019/6/15.
 */
@Controller
public class ThymeleafController {

    @RequestMapping(value = "/")
    @ApiOperation(value = "主页Index", notes = "")
    public String index(ModelMap map){
        // 加入一个属性，用来在模板中读取
        map.addAttribute("host", "http://blog.yyh.com");
        // return模板文件的名称，对应src/main/resources/templates/index.html
        return "index";
    }
}
