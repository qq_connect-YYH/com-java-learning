package com.example.demo.controller;

import com.example.demo.xmlvo.UserXml;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2019/6/15.
 */
@Controller
public class UserXmlController {

    @PostMapping(value = "/userxml",
            consumes = MediaType.APPLICATION_XML_VALUE,
            produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public UserXml create(@RequestBody UserXml user) {
        user.setName("com.example.demo.xmlvo.UserXml : " + user.getName());
        user.setAge(user.getAge() + 100);
        return user;
    }
}
