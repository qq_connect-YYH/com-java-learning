package com.example.demo.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * Created by Administrator on 2019/6/12.
 */
@Data
public class User {

    @ApiModelProperty("id")
    @NotNull
    private Long id;

    @ApiModelProperty("姓名")
    @Size(max = 20)
    @Pattern(regexp = "^.*$")
    private String name;

    @ApiModelProperty("年龄")
    @Max(150)
    @Min(0)
    private Integer age;

}
