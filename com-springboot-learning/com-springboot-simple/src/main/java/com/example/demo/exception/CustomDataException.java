package com.example.demo.exception;

/**
 * Created by Administrator on 2019/6/15.
 */
public class CustomDataException extends RuntimeException {

    public CustomDataException(String msg){
        super(msg);
    }
}
