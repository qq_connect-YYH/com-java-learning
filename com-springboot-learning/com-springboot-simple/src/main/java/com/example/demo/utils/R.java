/*
 *
 *      Copyright (c) 2018-2025, Aukey IT All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the trob4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Aukey IT (AukeyIT@aukeys.com)
 *
 */

package com.example.demo.utils;

import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @author Administrator
 * @param <T>
 */
@Builder
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int code = 1;

    @Getter
    @Setter
    private String msg = "success";


    @Getter
    @Setter
    private T data;

    public R() {
        super();
    }

    public static R success() {
        return new R();
    }

    public static R fall() {
        R fall = success();
        fall.setCode(0);
        return fall;
    }

    public static R success(Object data) {
        R success = success();
        success.setData(data);
        return success;
    }
    public static R success(String msg, Object data) {
        R success = success(data);
        success.setMsg(msg);
        return success;
    }

    public static R fall(String msg) {
        R fall = fall();
        fall.setMsg(msg);
        return fall;
    }

    public static R fall(String msg, Object data) {
        R fall = fall(msg);
        fall.setData(data);
        return fall;
    }

}
