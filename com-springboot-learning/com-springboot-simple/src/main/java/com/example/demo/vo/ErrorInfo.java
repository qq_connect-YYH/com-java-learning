package com.example.demo.vo;

import lombok.Data;

/**
 * Created by Administrator on 2019/6/15.
 */
@Data
public class ErrorInfo<T> {

    public static final Integer OK = 1;
    public static final Integer ERROR = 0;

    private Integer code;
    private String message;
    private String url;
    private T data;

}
