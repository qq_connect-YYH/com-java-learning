package com.example.demo.xmlvo;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Administrator on 2019/6/15.
 */
//生成get、set以及构造函数
@Data
@NoArgsConstructor
@AllArgsConstructor
//维护对象属性在xml中的对应关系
@JacksonXmlRootElement(localName = "UserXml")
public class UserXml {

    //维护对象属性在xml中的对应关系
    @JacksonXmlProperty(localName = "name")
    private String name;
    @JacksonXmlProperty(localName = "age")
    private Integer age;
}
