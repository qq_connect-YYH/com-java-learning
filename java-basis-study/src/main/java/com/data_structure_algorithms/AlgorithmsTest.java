package com.data_structure_algorithms;

import java.util.Arrays;

/**
 * 算法
 * Created by Administrator on 2019/8/16.
 */
public class AlgorithmsTest {

    private static int[] nums ;
    static {
        nums = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17};
    }

    public static void main(String[] args) {
        System.out.println(binarySearch(nums, 6));
        System.out.println(leftBound(nums, 20));
        System.out.println(rightBound(nums, 20));
    }

    //二分查找法
    private static int binarySearch(int[] nums, int target) {
        Arrays.sort(nums);
        int head = 0;
        int tail = nums.length -1;
        while (head <= tail){
//            int i = (tail - head) / 2 + head;
            int mid = (tail + head) / 2;
            if(nums[mid] == target){
                return mid;
            }else if(nums[mid] > target){
                tail = mid - 1;
            }else if(nums[mid] < target){
                head = mid + 1;
            }
        }
        return -1;
    }

    //寻找左侧边界的二分搜索
    static int leftBound(int[] nums, int target) {
        if (nums.length == 0)
            return -1;
        int left = 0;
        int right = nums.length; // 注意

        while (left < right) { // 注意
            int mid = (left + right) / 2;
            if (nums[mid] == target) {
                right = mid;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else if (nums[mid] > target) {
                right = mid; // 注意
            }
        }
        return left;
    }

    //寻找右侧边界的二分查找
    static int rightBound(int[] nums, int target) {
        if (nums.length == 0)
            return -1;
        int left = 0, right = nums.length;

        while (left < right) {
            int mid = (left + right) / 2;
            if (nums[mid] == target) {
                left = mid + 1; // 注意
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else if (nums[mid] > target) {
                right = mid;
            }
        }
        return left - 1; // 注意
    }


}
