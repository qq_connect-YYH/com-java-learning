package com.data_structure_algorithms;

import java.util.*;

/**
 *为什么？ 重写equals方法需同时重写hashCode方法
 * Created by Administrator on 2019/8/15.
 */
public class StructureTest {

    private static class Person{
        int idCard;
        String name;

        public Person(int idCard, String name) {
            this.idCard = idCard;
            this.name = name;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()){
                return false;
            }
            Person person = (Person) o;
            //两个对象是否等值，通过idCard来确定
            return this.idCard == person.idCard;
        }

//        @Override
//        public int hashCode() {
//            int result = idCard;
//            result = 31 * result + name.hashCode();
//            return result;
//        }
    }
    public static void main(String []args){
        final int HASH_BITS = 0x7fffffff;
        System.out.println(HASH_BITS);
        HashMap<Person,String> map = new HashMap<>(6);
        Person person = new Person(1234,"乔峰");
        map.put(person,"天龙八部");

        //get取出，从逻辑上讲应该能输出“天龙八部”
        System.out.println("结果:"+map.get(person));
        System.out.println("结果:"+map.get(new Person(1234,"乔峰")));

        System.out.println(Math.floor(6.5));

        TreeMap<String, Integer> treeMap = new TreeMap<>();
        treeMap.put("5", 5);
        treeMap.put("6", 6);
        treeMap.put("7", 7);
        treeMap.put("8", 8);
        treeMap.put("9", 9);
        System.out.println(treeMap);
        Set<Map.Entry<String, Integer>> entries = treeMap.entrySet();
        System.out.println(entries);
        Set<String> keySet = treeMap.keySet();
        System.out.println(keySet);
        Collection<Integer> values = treeMap.values();
        System.out.println(values);


        TreeSet<String> treeSet = new TreeSet<>( );
        treeSet.add("1");
        treeSet.add("2");
        System.out.println(treeSet);

    }

}
