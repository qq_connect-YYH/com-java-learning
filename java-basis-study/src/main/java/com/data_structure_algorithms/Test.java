package com.data_structure_algorithms;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by Administrator on 2019/8/15.
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(2 << 1);
        System.out.println(1 << 4);

        System.out.println(1 >>> 1);
        System.out.println(1%5);
        LinkedList<String> queue = new LinkedList<>();
        boolean offer = queue.offer("1");
        System.out.println(offer);
        String poll2 = queue.poll();

        ArrayBlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        boolean add1 = blockingQueue.add("1");
        boolean add2 = blockingQueue.add("2");
        boolean add3 = blockingQueue.add("3");
        blockingQueue.add("3");
        blockingQueue.offer("3");
        System.out.println(blockingQueue);

        //消费一个
        String poll = blockingQueue.poll();
        System.out.println(poll);
        //加入消费失败
        if(true){
            boolean offer1 = blockingQueue.offer(poll);
            System.out.println(blockingQueue);
        }

        PriorityBlockingQueue priorityBlockingQueue = new PriorityBlockingQueue(2);
        priorityBlockingQueue.put("3");
        boolean offer1 = priorityBlockingQueue.offer("1");
        boolean add = priorityBlockingQueue.add("2");
        priorityBlockingQueue.add("6");

        System.out.println(priorityBlockingQueue);

        Object remove = priorityBlockingQueue.remove();
        System.out.println(remove);


        Object poll1 = priorityBlockingQueue.poll();
        System.out.println(poll1);
        System.out.println(priorityBlockingQueue);

        Object peek = priorityBlockingQueue.element();
    }
}
