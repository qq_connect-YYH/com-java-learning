package com.javas.basis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2019/9/5.
 */
public class GCTest {
    public static void main(String[] args) {
        System.out.println("ClassLodarDemo's ClassLoader is " + GCTest.class.getClassLoader());
        System.out.println("The Parent of ClassLodarDemo's ClassLoader is " + GCTest.class.getClassLoader().getParent());
        System.out.println("The GrandParent of ClassLodarDemo's ClassLoader is " + GCTest.class.getClassLoader().getParent().getParent());
        List list = new ArrayList();
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(11);
        System.out.println(list);

        HashSet set = new HashSet();
        List newList = new ArrayList();
        set.addAll(list);
        newList.addAll(set);
        System.out.println(newList);
    }
}
