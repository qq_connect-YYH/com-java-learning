package com.javas.basis;

import cn.hutool.json.JSONUtil;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * Created by Administrator on 2019/8/7.
 */
public class MyClassTest {

    static {
        System.out.println("静态代码块");
    }

    {
        System.out.println("非静态代码块");
    }

    MyClassTest(){
        System.out.println("构造方法");
    }

    public static void test(){
        System.out.println("静态方法");
        {
            System.out.println("静态方法里的代码块");
        }
    }

    public static void main(String[] args) {
        // 获取 Java 线程管理 MXBean
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        // 不需要获取同步的 monitor 和 synchronizer 信息，仅获取线程和线程堆栈信息
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        // 遍历线程信息，仅打印线程 ID 和线程名称信息
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println("[" + threadInfo.getThreadId() + "] " + threadInfo.getThreadName());
        }
    }



}
