package com.javas.basis;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;

/**
 * Created by Administrator on 2019/8/11.
 */
public class Test {

    public static void main(String[] args) {
        /*
         * 运算符：移位运算符 　　
         * 简介：移位运算符就是在二进制的基础上对数字进行平移。
         * 按照平移的方向和填充数字的规则分为三种:<<(左移)、>>(带符号右移)和>>>(无符号右移)。 　　
         * 作用：对于大数据的2进制运算,位移运算符比那些普通运算符的运算要快很多
         * ,因为程序仅仅移动一下而已,不去计算,这样提高了效率,节省了资源
         * 比如这里：int newCapacity = oldCapacity + (oldCapacity >> 1); 右移一位相当于除2，右移n位相当于除以 2 的 n 次方。
         */
        System.out.println(Integer.MAX_VALUE);
        System.out.println(1 << 30);
        System.out.println(9 >> 1);
        //9 除以 2的 N平方
        System.out.println(1 << 4);
        //2 的 4平方
        System.out.println(4 ^ 2);
        HashMap hashMap = new HashMap(100);

//        for (int i=0; i < 10000; i++){
//            String str = RandomStringUtils.random(100,"UTF-8");
//            if(i>99996){
//                hashMap.put(str, str + i);
//            }else{
//                hashMap.put(str, str + i);
//            }
//        }
        hashMap.put("ffa","2");
        System.out.println(hashMap);
    }
}
