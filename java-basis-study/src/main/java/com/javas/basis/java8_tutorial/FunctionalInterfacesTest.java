package com.javas.basis.java8_tutorial;

import cn.hutool.json.JSONUtil;
import lombok.Data;

/**
 * 方法和构造函数引用
 * Created by Administrator on 2019/8/14.
 */
public class FunctionalInterfacesTest {

    @FunctionalInterface
    public interface Converter<F, T> {
        T convert(F from);
    }

    static class Something {
        String startsWith(String s) {
            return String.valueOf(s.charAt(0));
        }
    }

    @Data
    static class Person {
        String firstName;
        String lastName;
        Person() {}
        Person(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }

    interface PersonFactory<P extends Person> {
        P create(String firstName, String lastName);
    }

    public static void main(String[] args) {

        // TODO 将数字字符串转换为整数类型
        Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
        Integer converted = converter.convert("123");
        //class java.lang.Integer
        System.out.println(converted.getClass());

        //通过静态方法引用来表示
        Converter<String, Integer> converter2 = Integer::valueOf;
        Integer converted2 = converter2.convert("123");
        System.out.println(converted2.getClass());

        //通过对象引用
        Something something = new Something();
        Converter<String, String> converter3 = something::startsWith;
        String converted3 = converter3.convert("123");
        System.out.println(converted3);

        //造函数是如何使用::关键字来引用的
        //我们只需要使用 Person::new 来获取Person类构造函数的引用，Java编译器会自动根据PersonFactory.create方法的参数类型来选择合适的构造函数。
        PersonFactory<Person> personFactory = Person::new;
        Person person = personFactory.create("Peter", "Parker");
        System.out.println(JSONUtil.toJsonStr(person));
    }

}
