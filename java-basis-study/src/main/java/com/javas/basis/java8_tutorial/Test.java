package com.javas.basis.java8_tutorial;

import java.util.*;

/**
 * Created by Administrator on 2019/8/14.
 */
public class Test {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(3, "a");
        map.put(3, "b");
        System.out.println(map);

        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");

        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return b.compareTo(a);
            }
        });

        Collections.sort(names, (String a, String b) -> a.compareTo(b));

        names.sort( (String a, String b) -> b.compareTo(a));

        System.out.println(names);
    }
}
