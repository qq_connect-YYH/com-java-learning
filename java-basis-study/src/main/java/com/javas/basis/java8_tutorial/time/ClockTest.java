package com.javas.basis.java8_tutorial.time;

import java.time.Clock;
import java.time.Instant;
import java.util.Date;

/**
 * Created by Administrator on 2019/8/14.
 */
public class ClockTest {

    /**
     * Clock 类提供了访问当前日期和时间的方法，Clock 是时区敏感的，可以用来取代 System.currentTimeMillis() 来获取当前的微秒数。
     * 某一个特定的时间点也可以使用 Instant 类来表示，Instant 类也可以用来创建旧版本的java.util.Date 对象。
     * @param args
     */
    public static void main(String[] args) {
        Clock clock = Clock.systemDefaultZone();
        long millis = clock.millis();
        //1552379579043
        System.out.println(millis);
        Instant instant = clock.instant();
        System.out.println(instant);
        //2019-03-12T08:46:42.588Z
        Date legacyDate = Date.from(instant);
        //Tue Mar 12 16:32:59 CST 2019
        System.out.println(legacyDate);
    }
}
