package com.javas.basis.java8_tutorial.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Created by Administrator on 2019/8/14.
 */
public class LocalDateTest {

    public static void main(String[] args) {
        //获取现在的日期
        LocalDate today = LocalDate.now();
        System.out.println("今天的日期: "+today);

        LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
        System.out.println("明天的日期: "+tomorrow);

        LocalDate yesterday = tomorrow.minusDays(2);
        System.out.println("昨天的日期: "+yesterday);

        LocalDate independenceDay = LocalDate.of(2019, Month.MARCH, 12);
        DayOfWeek dayOfWeek = independenceDay.getDayOfWeek();
        //TUESDAY
        System.out.println("今天是周几:" + dayOfWeek);


        /*
        从字符串解析一个 LocalDate 类型和解析 LocalTime 一样简单,下面是使用  DateTimeFormatter 解析字符串的例子：
         */
        String str1 = "2014==04==12 01时06分09秒";
        // 根据需要解析的日期、时间字符串定义解析所用的格式器
        DateTimeFormatter fomatter1 = DateTimeFormatter.ofPattern("yyyy==MM==dd HH时mm分ss秒");

        LocalDateTime dt1 = LocalDateTime.parse(str1, fomatter1);
        // 输出 2014-04-12T01:06:09
        System.out.println(dt1);

        String str2 = "2014$$$四月$$$13 20小时";
        DateTimeFormatter fomatter2 = DateTimeFormatter.ofPattern("yyy$$$MMM$$$dd HH小时");
        LocalDateTime dt2 = LocalDateTime.parse(str2, fomatter2);
        // 输出 2014-04-13T20:00
        System.out.println(dt2);


        /**
         * 格式化日期
         */
        LocalDateTime rightNow = LocalDateTime.now();
        String date=DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(rightNow);
        //2019-03-12T16:26:48.29
        System.out.println(date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss");
        //2019-03-12 16:26:48
        System.out.println(formatter.format(rightNow));
    }
}
