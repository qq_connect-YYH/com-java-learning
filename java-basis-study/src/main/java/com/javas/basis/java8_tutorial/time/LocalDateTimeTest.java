package com.javas.basis.java8_tutorial.time;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Date;

/**
 * Created by Administrator on 2019/8/14.
 */
public class LocalDateTimeTest {

    /**
     * LocalDateTime 同时表示了时间和日期，相当于前两节内容合并到一个对象上了。
     * LocalDateTime 和 LocalTime还有 LocalDate 一样，都是不可变的。
     * LocalDateTime 提供了一些能访问具体字段的方法。
     * @param args
     */
    public static void main(String[] args) {
        LocalDateTime sylvester = LocalDateTime.of(2014, Month.DECEMBER, 31, 23, 59, 59);

        DayOfWeek dayOfWeek = sylvester.getDayOfWeek();
        // WEDNESDAY
        System.out.println(dayOfWeek);

        Month month = sylvester.getMonth();
        // DECEMBER
        System.out.println(month);

        long minuteOfDay = sylvester.getLong(ChronoField.MINUTE_OF_DAY);
        // 1439
        System.out.println(minuteOfDay);


        /*
        只要附加上时区信息，就可以将其转换为一个时间点Instant对象，Instant时间点对象可以很容易的转换为老式的
         */
        Instant instant = sylvester.atZone(ZoneId.systemDefault()).toInstant();
        Date legacyDate = Date.from(instant);
        // Wed Dec 31 23:59:59 CET 2014
        System.out.println(legacyDate);


        /*
        格式化LocalDateTime和格式化时间和日期一样的，除了使用预定义好的格式外，我们也可以自己定义格式：
         */

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss");
//        LocalDateTime parsed = LocalDateTime.parse("2019-08-14T18:58:37.437", formatter);
        LocalDateTime parsed = LocalDateTime.parse("2019-08-14T18:58:37.437");
        String string = formatter.format(parsed);
        System.out.println(string);

    }
}
