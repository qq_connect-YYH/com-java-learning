package com.javas.basis.java8_tutorial.time;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

/**
 * Created by Administrator on 2019/8/14.
 */
public class LocalTimeTest {

    /**
     * LocalTime 定义了一个没有时区信息的时间，例如 晚上10点或者 17 3015。
     * 之后比较时间并以小时和分钟为单位计算两个时间的时间差：
     * @param args
     */
    public static void main(String[] args) {
        ZoneId zone1 = ZoneId.of("Europe/Berlin");
        ZoneId zone2 = ZoneId.of("Brazil/East");

        LocalTime now1 = LocalTime.now(zone1);
        LocalTime now2 = LocalTime.now(zone2);
        // false
        System.out.println(now1.isBefore(now2));

        long hoursBetween = ChronoUnit.HOURS.between(now1, now2);
        long minutesBetween = ChronoUnit.MINUTES.between(now1, now2);

        // -3
        System.out.println(hoursBetween);
        // -239
        System.out.println(minutesBetween);



        //LocalTime 提供了多种工厂方法来简化对象的创建，包括解析时间字符串.
        LocalTime late = LocalTime.of(23, 59, 59);
        // 23:59:59
        System.out.println(late);
        DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(Locale.GERMAN);

        LocalTime leetTime = LocalTime.parse("13:37", germanFormatter);
        // 13:37
        System.out.println(leetTime);
    }
}
