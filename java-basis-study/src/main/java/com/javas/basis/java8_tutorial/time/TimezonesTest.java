package com.javas.basis.java8_tutorial.time;

import java.time.ZoneId;

/**
 * Created by Administrator on 2019/8/14.
 */
public class TimezonesTest {

    /**
     * 在新API中时区使用 ZoneId 来表示。时区可以很方便的使用静态方法of来获取到。
     * 抽象类ZoneId（在java.time包中）表示一个区域标识符。
     * 它有一个名为getAvailableZoneIds的静态方法，它返回所有区域标识符。
     * @param args
     */
    public static void main(String[] args) {
        //输出所有区域标识符
        System.out.println(ZoneId.getAvailableZoneIds());

        ZoneId zone1 = ZoneId.of("Europe/Berlin");
        ZoneId zone2 = ZoneId.of("Brazil/East");
        System.out.println(zone1.getRules());// ZoneRules[currentStandardOffset=+01:00]
        System.out.println(zone2.getRules());// ZoneRules[currentStandardOffset=-03:00]
    }
}
