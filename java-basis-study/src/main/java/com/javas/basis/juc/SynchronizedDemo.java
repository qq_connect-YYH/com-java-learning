package com.javas.basis.juc;

/**
 * Created by Administrator on 2019/8/11.
 */
public class SynchronizedDemo {
    /*
    JDK 自带的 javap 命令查看 SynchronizedDemo 类的相关字节码信息：
    首先切换到类的对应目录执行 javac SynchronizedDemo.java 命令生成编译后的 .class 文件，
    然后执行javap -c -s -v -l SynchronizedDemo.class
     */

    //synchronized 同步语句块的情况
    public void method() {
        synchronized (this) {
            System.out.println("synchronized 代码块");
        }
    }

    //synchronized 修饰方法的的情况F
    public synchronized void method2() {
        System.out.println("synchronized 方法");
    }

    public static void main(String[] args) {
        SynchronizedDemo demo = new SynchronizedDemo();
        while (true){
            demo.method();
        }
    }
}
