package com.javas.basis.juc.auomic;

import lombok.Data;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * 对象的属性修改类型原子类
 * Created by Administrator on 2019/8/13.
 */
public class AtomicIntegerFieldUpdaterTest {

    public static void main(String[] args) {
        AtomicIntegerFieldUpdater<User> a = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");

        User user = new User("Java", 22);
        // 22
        System.out.println(a.getAndIncrement(user));
        // 23
        System.out.println(a.get(user));
    }

    @Data
    static class User {
        private String name;
        public volatile int age;
        public User(String name, int age) {
            super();
            this.name = name;
            this.age = age;
        }
    }
}
