package com.javas.basis.juc.auomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 基本数据类型原子类
 * Created by Administrator on 2019/8/11.
 */
public class AtomicIntegerTest {
    /*
     * AtomicInteger 类的原理
     AtomicInteger 类主要利用 CAS (compare and swap) + volatile 和 native 方法来保证原子操作，
     从而避免 synchronized 的高开销，执行效率大为提升。
     CAS的原理是拿期望的值和原本的一个值作比较，如果相同则更新成新的值。
     UnSafe 类的 objectFieldOffset() 方法是一个本地方法，这个方法是用来拿到“原来的值”的内存地址，返回值是 valueOffset。
     另外 value 是一个volatile变量，在内存中可见，因此 JVM 可以保证任何时刻任何线程总能拿到该变量的最新值。
     */

    private AtomicInteger count = new AtomicInteger();

    //使用AtomicInteger之后，不需要对该方法加锁，也可以实现线程安全。
    public void increment() {
        count.incrementAndGet();
    }

    public int getCount() {
        return count.get();
    }

}
