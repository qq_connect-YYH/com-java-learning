package com.javas.basis.juc.auomic;

import lombok.Data;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 引用类型原子类
 * Created by Administrator on 2019/8/13.
 */
public class AtomicReferenceTest {

    public static void main(String[] args) {
        AtomicReference<Person> ar = new AtomicReference<Person>();

        Person person = new Person("SnailClimb", 22);
        ar.set(person);

        Person updatePerson = new Person("Daisy", 20);
        ar.compareAndSet(person, updatePerson);

        System.out.println(ar.get().getName());
        System.out.println(ar.get().getAge());
    }

    @Data
    static class Person {
        private String name;
        private int age;

        public Person(String name, int age) {
            super();
            this.name = name;
            this.age = age;
        }
    }
}
