package com.javas.basis.juc.auomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 原子性问题
 * Created by Administrator on 2019/8/7.
 */
public class TestAtomicDemo {

    public static void main(String[] args){
        AtomicDemo ad = new AtomicDemo();
        for(int i=0; i < 10; i++){
            new Thread(ad).start();
        }

    }

    static class AtomicDemo implements Runnable{
        //    private int serialNumber = 0;
        //使用原子变量
        private AtomicInteger serialNumber = new AtomicInteger();

        public void run(){
            try{
                Thread.sleep(200);
            }catch(InterruptedException e){

            }
            System.out.println(Thread.currentThread().getName() + ":" + this.getSerialNumber());
        }
        public int getSerialNumber(){
//        return serialNumber++;
            //// 自增运算
            return serialNumber.getAndIncrement();
        }
    }
}

