package com.javas.basis.juc.concurrent;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 三个线程T1 T2 T3 按顺序输出
 * @author y.yh
 * @create 2019/9/16 16:46
 */
public class ReentrantLockABC {

    public static void main(String[] args) {
        Example demo = new Example();
        new Thread( () -> demo.outB(), "B" ).start();
        new Thread( () -> demo.outC(), "C" ).start();
        new Thread( () -> demo.outA(), "A" ).start();

    }

    static class Example{
        private volatile int num = 1;
        ReentrantLock lock = new ReentrantLock();
        Condition conditionA = lock.newCondition();
        Condition conditionB = lock.newCondition();
        Condition conditionC = lock.newCondition();


        public void outA(){
            lock.lock();
            try {
                if(num != 1){
                    conditionA.await();
                }
                System.out.println("当前线程 " + Thread.currentThread().getName());
                num = 2;
                conditionB.signalAll();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }

        public void outB(){
            lock.lock();
            try {
                if(num != 2){
                    conditionB.await();
                }
                System.out.println("当前线程 " + Thread.currentThread().getName());
                num = 3;
                conditionC.signalAll();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }

        public void outC(){
            lock.lock();
            try {
                if(num != 3){
                    conditionC.await();
                }
                System.out.println("当前线程 " + Thread.currentThread().getName());
                num = 1;
                conditionA.signalAll();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
    }
}
