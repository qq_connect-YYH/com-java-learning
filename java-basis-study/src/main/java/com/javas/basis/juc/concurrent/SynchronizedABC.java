package com.javas.basis.juc.concurrent;

/**
 * 三个线程T1 T2 T3 按顺序输出
 * @author y.yh
 * @create 2019/9/16 16:59
 */
public class SynchronizedABC {

    public static void main(String[] args) {
        Example demo = new Example();
        new Thread( () -> demo.outB(), "B" ).start();
        new Thread( () -> demo.outC(), "C" ).start();
        new Thread( () -> demo.outA(), "A" ).start();

    }

    static class Example{
        private volatile int num = 1;
        public void outA(){
            synchronized (this){
                if(num != 1){
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("当前线程 " + Thread.currentThread().getName());
                num = 2;
                this.notifyAll();
            }
        }
        public void outB(){
            synchronized (this){
                if(num != 2){
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("当前线程 " + Thread.currentThread().getName());
                num = 3;
                this.notifyAll();
            }
        }
        public void outC(){
            synchronized (this){
                if(num != 3){
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("当前线程 " + Thread.currentThread().getName());
                num = 1;
                this.notifyAll();
            }
        }
    }
}
