package com.javas.basis.juc.concurrent;

import java.util.concurrent.CountDownLatch;

/**
 * 计算多线程的执行时间
 * 是一个同步辅助类,在完成一组正在其他线程中执行的操作之前,它允许一个或多个线程一直等待;
 * Created by Administrator on 2019/8/7.
 */
public class TestCountDownLatch {
    public static void main(String[] args){
        final CountDownLatch latch = new CountDownLatch(10);
        LatchDemo ld = new LatchDemo(latch);

        long start = System.currentTimeMillis();

        // 创建10个线程
        for(int i=0; i<10; i++){
            new Thread(ld).start();
        }

        try{
            latch.await();
        }catch(InterruptedException e){
            System.out.println("e:"+ e);
        }

        long end = System.currentTimeMillis();

        System.out.println("耗费时间为:"+(end - start));

    }
}

class LatchDemo implements Runnable{
    private CountDownLatch latch;

    // 有参构造器
    public LatchDemo(CountDownLatch latch){
        this.latch = latch;
    }

    public void run(){
        synchronized(this){
            try{
                // 打印50000以内的偶数
                for(int i=0; i<10; i++){
                    if(i % 2 == 0){
                        String name = Thread.currentThread().getName();
                        System.out.println(name +"->"+i);
                    }
                }
            }finally{
                // 线程数量递减
                latch.countDown();
            }
        }
    }
}