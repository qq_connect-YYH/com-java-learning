package com.javas.basis.juc.concurrent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
  测试类: 以卖票为例
 * Created by Administrator on 2019/8/7.
 */
public class TestLock {

    public static void main(String[] args){
        Ticket ticket = new Ticket();
        new Thread(ticket,"1号窗口").start();
        new Thread(ticket,"2号窗口").start();
        new Thread(ticket,"3号窗口").start();
    }
}

//  使用 lock 之前 没有同步功能 会出现问题
/*
  class Ticket implements Runnable{
    private int tick = 100;
    public void run(){
        while(true){
            if(tick > 0){
                try{
                    Thread.sleep(200);
                }catch(InterruptedException e){

                }
                System.out.println(Thread.currentThread().getName()+"完成售票,余票为: "+ --tick);
            }
            if(tick == 0)
                break;
        }
    }
}*/

// 使用 Lock
class Ticket implements Runnable{
    private int tick = 100;
    private Lock lock = new ReentrantLock();
    public void run(){
        while(true){
            // 上锁
            lock.lock();
            try{
                if(tick > 0){
                    try{
                        Thread.sleep(200);
                    }catch(InterruptedException e){

                    }
                    System.out.println(Thread.currentThread().getName()+"完成售票,余票为: "+ --tick);
                }
                if(tick == 0)
                    break;
            }finally{
                // 释放锁
                lock.unlock();
            }
        }
    }
}