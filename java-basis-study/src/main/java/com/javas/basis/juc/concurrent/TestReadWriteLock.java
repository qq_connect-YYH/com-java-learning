package com.javas.basis.juc.concurrent;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁
 * Created by Administrator on 2019/8/7.
 */
public class TestReadWriteLock {

    public static void main(String[] args){
        ReadWriteLockDemo rw = new ReadWriteLockDemo();

        // 一个线程进行写
        new Thread( () ->{
            rw.set((int)(Math.random()*100));
        },"Write:").start();

        // 100个线程进行读操作
        for(int i=0; i<10; i++){
            new Thread( () ->{
                rw.get();
            },"Read:").start();
        }
    }
}

class ReadWriteLockDemo{
    private int number = 0;
    private ReadWriteLock lock = new ReentrantReadWriteLock();
    // 读
    public void get(){
        // 上锁
        lock.readLock().lock();
        try{
            System.out.println(Thread.currentThread()+":"+number);
        }finally{
            // 释放锁
            lock.readLock().unlock();
        }
    }

    // 写
    public void set(int number){
        lock.writeLock().lock();
        try{
            System.out.println(Thread.currentThread());
            this.number = number;
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally{
            lock.writeLock().unlock();
        }
    }
}
