package com.javas.basis.juc.producer_consumer;

import lombok.Data;

/**
 * 通过Synchronized同步 蛋糕的 生产者 消费者
 * Created by Administrator on 2019/8/28.
 */
public class ProducerConsumerCakeToSynchronized {
    public static void main(String[] args) throws Exception {
        Cake cake = new Cake("世纪蛋糕", 0);
        Producer producer = new Producer(cake);
        Consumer consumer = new Consumer(cake);

        int i = 10;
        while (i > 0){
            Thread proThread = new Thread(() -> {
                try {
                    producer.producerCake();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "生产者");
            proThread.start();

            Thread conThread = new Thread(() -> {
                try {
                    consumer.consumerCake();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "消费者");
            conThread.start();
            i--;
        }

        while (true){

        }
    }

    @Data
    static class Producer {
        private Cake cake;
        public Producer(Cake cake){
            this.cake = cake;
        }
        public void producerCake() throws InterruptedException {
            synchronized (cake){
                if(cake.getNum() > 0){
                    cake.wait();
                }
                int i=10;
                while (i > 0){
                    cake.setNum(cake.getNum()+1);
                    System.out.println(String.format("线程:%s,生产者生产了%s个蛋糕，总共%s个！", Thread.currentThread().getName()+"-"+Thread.currentThread().getId(),1, cake.getNum()));
                    i--;
                    cake.wait();
                }
                cake.notifyAll();
            }
        }
    }

    @Data
    static class Consumer {
        private Cake cake;
        public Consumer(Cake cake){
            this.cake = cake;
        }
        public void consumerCake() throws InterruptedException {
            synchronized (cake){
                if(cake.getNum() == 0){
                    cake.wait();
                }
                while (cake.getNum() > 0){
                    cake.setNum(cake.getNum()-1);
                    System.out.println(String.format("线程:%s,消费者消费了%s个蛋糕，还剩%s个！", Thread.currentThread().getName()+"-"+Thread.currentThread().getId(), 1, cake.getNum()));
                    cake.wait();
                }
                cake.notifyAll();
            }
        }
    }

    @Data
    static class Cake {
        //名称
        private String name;
        //数量 volatile 防止指令重排序、内存可见性（所有线程都到堆内存读取，不读取线程本地的缓存）
        private volatile int num;

        public Cake(){}
        public Cake(String name, int num){
            this.name = name;
            this.num = num;
        }
    }
}
