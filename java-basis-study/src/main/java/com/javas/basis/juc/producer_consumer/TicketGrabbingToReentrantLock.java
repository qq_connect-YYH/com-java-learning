package com.javas.basis.juc.producer_consumer;

import lombok.Data;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 通过ReentrantLock同步 抢票
 * Created by Administrator on 2019/8/28.
 */
public class TicketGrabbingToReentrantLock {


    public static void main(String[] args) throws Exception {
        Class<?> aClass = Class.forName("com.javas.basis.juc.producer_consumer.TicketGrabbingToReentrantLock");
//        ExecutorService executorService = Executors.newFixedThreadPool(6);
//
//        Ticket ticket = new Ticket("深圳-福州", 0);
//        for (int i=0; i<100; i++){
//            executorService.execute( () -> {
//                ticket.granbbing();
//            });
//        }
//
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        while (true){
//            Thread.sleep(1000);
//            singleThreadExecutor.execute( () -> {
//                ticket.producerTicket();
//            });
//        }
    }
    @Data
    static class Ticket {
        private ReentrantLock lock = new ReentrantLock();
        Condition producer = lock.newCondition();
        Condition consumer = lock.newCondition();

        //名称
        private String name;
        //数量 volatile 防止指令重排序、内存可见性（所有线程都到堆内存读取，不读取线程本地的缓存）
        private volatile int num;

        public Ticket(){}
        public Ticket(String name, int num){
            this.name = name;
            this.num = num;
        }

        public void granbbing(){
            lock.lock();
            try {
                if(this.num > 0){
                    System.out.println(String.format("线程：%s，抢到 1 张车票，总共:%s张车票，还剩：%s张车票！", Thread.currentThread().getName()+"-"+Thread.currentThread().getId(), this.num, --this.num));
                    Thread.sleep(50);
                    //没消费一次 请求生产者检查票数
                    producer.signalAll();
                }else{
                    consumer.await();
                }
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }

        public void producerTicket(){
            lock.lock();
            try {
                if(num > 0){
                    producer.await();
                }
                else{
                    System.out.println(String.format("线程：%s，重新生成了 100张车票", Thread.currentThread().getName()+"-"+Thread.currentThread().getId()));
                    this.num = 100;
                    consumer.signalAll();
                }
            }catch (Exception e){
               e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
    }
}
