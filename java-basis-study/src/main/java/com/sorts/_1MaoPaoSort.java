package com.sorts;

import java.util.Random;

/**
 * 冒泡排序
 * @author y.yh
 * @create 2019/9/9 11:56
 */
public class _1MaoPaoSort {
    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[20];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(100);
        }

        for (int i=0; i<arr.length-1; i++){
            for (int j=i+1; j< arr.length; j++){
                if(arr[i] > arr[j]){
                    arr[i] = arr[i]+arr[j];
                    arr[j] = arr[i]-arr[j];
                    arr[i] = arr[i]-arr[j];
                }
            }
        }
//        for (int i=0; i<arr.length; i++){
//            for (int j=0; j< arr.length-1-i; j++){
//                if(arr[j] > arr[j+1]){
//                    arr[j+1] = arr[j+1] + arr[j];
//                    arr[j] = arr[j+1] - arr[j];
//                    arr[j+1] = arr[j+1] - arr[j];
//                }
//            }
//        }
        for (int i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();
    }
}
