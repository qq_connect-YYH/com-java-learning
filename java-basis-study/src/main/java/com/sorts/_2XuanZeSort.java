package com.sorts;

import java.util.Random;

/**
 * 选择排序
 * @author y.yh
 * @create 2019/9/9 12:24
 */
public class _2XuanZeSort {
    public static void main(String[] args) {
        Random random = new Random();
        Integer[] arr = new Integer[21];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(200);
        }
        for (Integer i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();

        for (int i = 0; i < arr.length-1; i++) {
            //定位最小值索引
            int minIndex = i;
            for (int j = i+1; j < arr.length; j++) {
                if(arr[minIndex] > arr[j]){
                    minIndex = j;
                }
            }
            if(minIndex > i){
                arr[minIndex] = arr[minIndex] + arr[i];
                arr[i] = arr[minIndex] - arr[i];
                arr[minIndex] = arr[minIndex] - arr[i];
            }
        }
        for (int i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();
    }
}
