package com.sorts;

import sun.swing.icon.SortArrowIcon;

import java.util.Random;

/**
 * 插入排序
 * @author y.yh
 * @create 2019/9/9 14:36
 */
public class _3ChaRuSort {

    public static void main(String[] args) {
        Random random = new Random();
        Integer[] arr = new Integer[22];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(300);
        }
        for (Integer i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();

        //1.生成降序
//        for (int i = 0; i < arr.length; i++) {
//            //假设最大值索引
//            int maxIndex = i;
//            //理论上i-1 之前的元素代表已排序元素
//            for (int j = i-1; j >= 0 && j < i; j--) {
//                if(arr[maxIndex] > arr[j]){
//                    //求和
//                    arr[maxIndex] = arr[maxIndex] + arr[j];
//                    //和 - 最小 = 最大
//                    arr[j] = arr[maxIndex] - arr[j];
//                    //和 - 最大 = 最小
//                    arr[maxIndex] = arr[maxIndex] - arr[j];
//
//                    //最大值索引前移
//                    maxIndex = j;
//                }
//            }
//        }

        //2. 生成升序
        int currentVal;
        for (int i = 0; i < arr.length - 1; i++) {
            //当前要插入的值
            currentVal = arr[i + 1];
            //要比较的索引
            int preIndex = i;
            while (preIndex >= 0 && currentVal < arr[preIndex]){
                arr[preIndex + 1] = arr[preIndex];
                preIndex--;
            }
            arr[preIndex + 1] = currentVal;
        }

        for (Integer i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();

        int[] sortarr = new int[]{1,3,4,6,8,6,0,2,4,565,165,666,888,999,6868,999};
        chaRuSort(sortarr);
        for (Integer i : sortarr) {
            System.out.print(i+" ");
        }
        System.out.println();
    }

    public static void chaRuSort(int[] arr){
        if(arr == null || arr.length == 0)
            return;
        //当前要比较的值
        int currentVal;
        //当前被比较的索引
        int preIndex;
        //升序
        for (int i = 0; i < arr.length-1; i++) {
            currentVal = arr[i+1];
            preIndex = i;
            while (preIndex >=0 && arr[preIndex] > currentVal){
                //大的值后移
                arr[preIndex + 1] = arr[preIndex];
                //当前被比较的索引前移
                preIndex--;
            }
            //当前要比较的值赋值
            arr[preIndex + 1] = currentVal;
        }
    }
}
