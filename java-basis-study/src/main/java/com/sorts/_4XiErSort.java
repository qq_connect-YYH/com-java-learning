package com.sorts;

import java.util.Random;

/**
 * 希尔排序 利用增量+插入排序
 * @author y.yh
 * @create 2019/9/9 15:35
 */
public class _4XiErSort {

    public static void main(String[] args) {
        Random random = new Random();
        Integer[] arr = new Integer[23];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(300);
        }
        for (Integer i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();

        int temp;
        //数组长度
        int len = arr.length;
        //增量
        int gap = arr.length/2;

        while (gap > 0){
            for (int i = gap; i < len; i++) {
                temp = arr[i];
                int preIndex = i - gap;
                while (preIndex >= 0 && arr[preIndex] > temp) {
                    arr[preIndex + gap] = arr[preIndex];
                    preIndex = preIndex - gap;
                }
                arr[preIndex + gap] = temp;
            }
            gap /= 2;
        }

        for (Integer i : arr) {
            System.out.print(i+" ");
        }
        System.out.println();

        int[] xier = new int[] {1,-2,-9,66,99,88,36,96,98,68,68,6,87,6895,68};
        xiErSort(xier);
        for (Integer i : xier) {
            System.out.print(i+" ");
        }
        System.out.println();
    }

    public static void xiErSort(int[] arr){
        if(arr==null || arr.length == 0)
            return;
        //初始增量值(当前比较插入的索引)
        int grap = arr.length / 2;
        int currentVal;
        int preIndex;
        while (grap > 0){
            for (int i = grap; i < arr.length; i++) {
                currentVal = arr[i];
                preIndex = i - grap;
                while (preIndex >=0 && arr[preIndex] > currentVal){
                    arr[preIndex + grap] = arr[preIndex];
                    preIndex -= grap;
                }
                arr[preIndex + grap] = currentVal;
            }
            //增量值
            grap /= 2;
        }
    }
}
