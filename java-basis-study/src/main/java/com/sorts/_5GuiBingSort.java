package com.sorts;

import java.util.Arrays;
import java.util.Random;

/**
 * 归并排序
 * @author y.yh
 * @create 2019/9/9 16:18
 */
public class _5GuiBingSort {

    private static final int INSERTIONSORT_THRESHOLD = 7;


    public static void main(String[] args) {
        Random random = new Random();
        int[] array = new int[24];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(300);
        }
        for (int i : array) {
            System.out.print(i+" ");
        }
        System.out.println();


        int mid = array.length / 2;
        int[] left = Arrays.copyOfRange(array, 0, mid);
        legacyMergeSort(left);
        int[] right = Arrays.copyOfRange(array, mid, array.length);
        legacyMergeSort(right);


        int[] merge = merge(left, right);
        for (Integer i : merge) {
            System.out.print(i+" ");
        }
        System.out.println();
    }
    private static void legacyMergeSort(int[] a) {
        int[] aux = a.clone();
        mergeSort(aux, a, 0, a.length, 0);
    }

    private static void mergeSort(int[] src, int[] dest, int low, int high, int off) {
        int length = high - low;

        // Insertion sort on smallest arrays
        if (length < INSERTIONSORT_THRESHOLD) {
            for (int i=low; i<high; i++)
                for (int j=i; j>low &&
                        ((Comparable) dest[j-1]).compareTo(dest[j])>0; j--)
                    swap(dest, j, j-1);
            return;
        }

        // Recursively sort halves of dest into src
        int destLow  = low;
        int destHigh = high;
        low  += off;
        high += off;
        int mid = (low + high) >>> 1;
        mergeSort(dest, src, low, mid, -off);
        mergeSort(dest, src, mid, high, -off);

        if (((Comparable)src[mid-1]).compareTo(src[mid]) <= 0) {
            System.arraycopy(src, low, dest, destLow, length);
            return;
        }

        // Merge sorted halves (now in src) into dest
        for(int i = destLow, p = low, q = mid; i < destHigh; i++) {
            if (q >= high || p < mid && ((Comparable)src[p]).compareTo(src[q])<=0)
                dest[i] = src[p++];
            else
                dest[i] = src[q++];
        }
    }
    private static void swap(int[] x, int a, int b) {
        int t = x[a];
        x[a] = x[b];
        x[b] = t;
    }

    /**
     * 归并排序——将两段排序好的数组结合成一个排序数组
     *
     * @param left
     * @param right
     * @return
     */
    public static int[] merge(int[] left, int[] right) {
        int[] result = new int[left.length + right.length];
        for (int index = 0, i = 0, j = 0; index < result.length; index++) {
            if (i >= left.length) {
                result[index] = right[j++];
            } else if (j >= right.length) {
                result[index] = left[i++];
            } else if (left[i] > right[j]) {
                result[index] = right[j++];
            } else {
                result[index] = left[i++];
            }
        }
        return result;
    }
}
