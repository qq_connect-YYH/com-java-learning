package springbootactivtidemo;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by qin.zl on 2018/10/19.
 */
public class Main {
    public static void main(String[] args) {
        HashSet hashSet = new HashSet(Arrays.asList("111", "111","222"));
        System.out.println(hashSet);
        String s = StringUtils.join(hashSet.toArray(), ",");
        System.out.println(s);
    }
}
