package springbootactivtidemo;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qin.zl on 2018/10/19.
 */
public class MultipleManagerDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map map = new HashMap();
        map.put("manager", "aaa,bbb");

        execution.setVariables(map);
    }
}
