package springbootactivtidemo.activiti.command;

import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;

/**
 * 跳回重新审批节点的执行器
 *
 * @author Administrator
 * @date 2019/7/15
 */
public class ReapplyNodeActivityCmd implements Command<Object>{

    private String activityId;
    private String executionId;
    private String taskId;


    public ReapplyNodeActivityCmd(String executionId, String activityId, String taskId) {
        this.activityId = activityId;
        this.executionId = executionId;
        this.taskId = taskId;
    }


    @Override
    public Object execute(CommandContext commandContext) {
        // TODO: 2019/7/16 如果是并行网关这里会有问题的！！！
        ExecutionEntity executionEntity = commandContext.getExecutionEntityManager().findExecutionById(executionId);

        TaskEntity task = commandContext.getTaskEntityManager().findTaskById(taskId);
        task.setExecutionId(null);
        task.update();

        commandContext.getTaskEntityManager().deleteTask(taskId, "删除当前节点，跳转到重新申请节点", true);

        ProcessDefinitionImpl processDefinition = executionEntity.getProcessDefinition();

        ActivityImpl activity = processDefinition.findActivity(activityId);

        executionEntity.executeActivity(activity);

        return executionEntity;
    }
}
