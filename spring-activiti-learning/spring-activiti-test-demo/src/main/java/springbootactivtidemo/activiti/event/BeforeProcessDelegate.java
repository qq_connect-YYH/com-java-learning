package springbootactivtidemo.activiti.event;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import java.util.Map;

/**
 * 流程开始做的事情
 * Created by Administrator on 2019/7/15.
 */
public class BeforeProcessDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String currentActivityName = execution.getCurrentActivityName();
        String eventName = execution.getEventName();
        System.out.println("流程开始事件");
        System.out.println("currentActivityName: "+ currentActivityName);
        System.out.println("eventName: "+ eventName);

        Map<String, Object> variables = execution.getVariables();


        execution.setVariables(variables);
    }
}
