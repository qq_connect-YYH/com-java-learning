package springbootactivtidemo.activiti.event;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import java.util.Map;

/**
 * 多实例任务节点 开始事件
 * Created by Administrator on 2019/7/16.
 */
public class MultiInstanceStartEventDelegate implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        DelegateExecution execution = delegateTask.getExecution();

        String currentActivityName = execution.getCurrentActivityName();
        String eventName = execution.getEventName();
        System.out.println("多任务节点开始事件");
        System.out.println("currentActivityName: "+ currentActivityName);
        System.out.println("eventName: "+ eventName);


        Map<String, Object> variables = delegateTask.getVariables();

        // TODO: 2019/7/16 并行会签存在几个用户审批，这个事件就会执行几次，看怎么优化 ？
        // TODO: 2019/7/16 遇到驳回重新审批，无法重置变量，直接重新多次复制 一样的？
        Object nrOfInstancesTotal = variables.get("nrOfInstancesTotal");
//        if(nrOfInstancesTotal == null){
            //总实例数量
            Object nrOfInstances = variables.get("nrOfInstances");
            variables.put("nrOfInstancesTotal", nrOfInstances);
//        }

        Object nrOfCompletedSuccessNum = variables.get("nrOfCompletedSuccessNum");
//        if(nrOfCompletedSuccessNum==null){
            //审核成功数量
            variables.put("nrOfCompletedSuccessNum", 0);
//        }
        System.out.println("variables --> "+ variables);
        delegateTask.setVariables(variables);
    }
}
