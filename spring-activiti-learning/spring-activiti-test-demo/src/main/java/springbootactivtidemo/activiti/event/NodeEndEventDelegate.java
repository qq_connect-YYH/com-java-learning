package springbootactivtidemo.activiti.event;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * Created by Administrator on 2019/7/15.
 */
public class NodeEndEventDelegate implements JavaDelegate {



    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String currentActivityName = execution.getCurrentActivityName();
        String eventName = execution.getEventName();
        System.out.println("节点结束事件");
        System.out.println("currentActivityName: "+ currentActivityName);
        System.out.println("eventName: "+ eventName);


    }
}
