package springbootactivtidemo.activiti.event;

import org.activiti.engine.EngineServices;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.task.Task;

import java.util.List;
import java.util.Map;

/**
 * 启动后节点做的事情
 * Created by Administrator on 2019/7/15.
 */
public class NodeStartEventDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String currentActivityName = execution.getCurrentActivityName();
        String eventName = execution.getEventName();
        System.out.println("节点开始事件");
        System.out.println("currentActivityName: "+ currentActivityName);
        System.out.println("eventName: "+ eventName);

        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables"+ variables);

        String id = execution.getId();
        String processInstanceId = execution.getProcessInstanceId();
        EngineServices engineServices = execution.getEngineServices();
        TaskService taskService = engineServices.getTaskService();
        RuntimeService runtimeService = engineServices.getRuntimeService();

        List<Task> list = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
//        Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
        System.out.println("tasks: "+ list);

        List<Execution> list1 = runtimeService.createExecutionQuery().processInstanceId(processInstanceId).list();
        System.out.println("Executions: "+ list);
    }
}
