package springbootactivtidemo;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import springbootactivtidemo.activiti.command.ReapplyNodeActivityCmd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

/**
 * 多任务实例节点测试
 * Created by Administrator on 2019/7/15.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootActivtiDemoApplication.class)
public class TestJointlySignProcess {

    @Autowired
    ManagementService managementService;
    @Autowired
    RepositoryService repositoryService;
    @Autowired
    RuntimeService runtimeService;
    @Autowired
    IdentityService identityService;
    @Autowired
    TaskService taskService;
    @Autowired
    ProcessEngine  processEngine ;



    /**
     * 部署流程
     * @throws FileNotFoundException
     */
    @Test
    public void deploy() throws FileNotFoundException {
        String deploymentName = "多任务实例";
        InputStream in = new FileInputStream("C:\\Users\\Administrator\\Desktop\\activity\\leaveJointlySign.zip");
        ZipInputStream zipInputStream = new ZipInputStream(in);
        Deployment deploy = repositoryService.createDeployment()
                .name(deploymentName)
                .addZipInputStream(zipInputStream)
//                .addClasspathResource("processes/expenseApply/Processes.bpmn")
//                .addClasspathResource("processes/expenseApply/Processes.png")
                .category("多任务实例节点测试")
                .deploy();
        System.out.println(deploy);
    }

    /**
     * 启动流程
     */
    @Test
    public void start(){
        Map<String, Object> variables = new HashMap();
        //发现skip expression不生效的就要注意开启这个
        //这个变量用来跳过某个节点,节点设置了 ${skipExpression} 变量即可。
        variables.put("_ACTIVITI_SKIP_EXPRESSION_ENABLED", true);
        variables.put("user", "test");
        variables.put("DeptManager", "2");
        variables.put("GroupPresident", "7");

        //多任务实例节点变量
        List<String> jointlySign = new ArrayList<>();
        jointlySign.add("jointlySign1");
        jointlySign.add("jointlySign2");
        jointlySign.add("jointlySign3");
        variables.put("assigneeList", jointlySign);


        //流程id
        String processDefinitionKey = "LeaveJointlySignApply";
        //业务单唯一标识   用来与流程绑定
        String businessKey = "JointlySign-1";
        runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey, variables);
        identityService.setAuthenticatedUserId(null);
    }

    /**
     * 签收执行实例
     */
    @Test
    public void signExecuteInstance(){
        String taskId = "137511";
        String userId = "JointlySign-1-"+ taskId;
        taskService.claim(taskId, userId);
    }

    /**
     * 委派转移任务
     */
    @Test
    public void delegateTask(){
        String taskId = "22505";
        String userId = "JointlySign-1-"+ taskId;
        taskService.delegateTask(taskId, userId);
    }

    /**
     * 审批执行实例
     * 场景：审批单节点
     */
    @Test
//    @Transactional
    public void completeExecuteInstance(){
        String taskId = "227511";

        Map<String, Object> variables = new HashMap<>();
        //该任务被委托时,需要先resolve任务 所属者
        if (StringUtils.isNotEmpty(taskService.createTaskQuery().taskId(taskId).singleResult().getOwner())) {
            taskService.resolveTask(taskId);
        }
        taskService.complete(taskId, variables);
    }

    /**
     * 多实例任务审批
     */
    @Test
//    @Transactional
    public void multiCompleteExecuteInstance(){
        //是否通过
        boolean isPass = true;
        String taskId = "220015";
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        Object nrOfCompletedSuccessNum = processEngine.getRuntimeService().getVariable(task.getExecutionId(), "nrOfCompletedSuccessNum");

        if(isPass){
            nrOfCompletedSuccessNum = Integer.parseInt(nrOfCompletedSuccessNum.toString()) + 1;
        }
        Map<String, Object> variables = new HashMap<>();
        variables.put("nrOfCompletedSuccessNum", nrOfCompletedSuccessNum);
        taskService.complete(taskId, variables);
    }

    /**
     * 驳回重新申请 直接跳到重新申请节点
     */
    @Test
//    @Transactional
    public void completeReapplyExecuteInstance(){
        String taskId = "150002";
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        managementService.executeCommand(new ReapplyNodeActivityCmd(task.getExecutionId(), "ReApply", taskId));
    }
}
