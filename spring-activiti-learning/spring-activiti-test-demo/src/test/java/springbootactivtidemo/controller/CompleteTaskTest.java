package springbootactivtidemo.controller;

import org.activiti.engine.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by qin.zl on 2018/9/27.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CompleteTaskTest {
    @Autowired
    TaskService taskService;

    @Test
    public void completeTask() {
        taskService.complete("25026");

        System.out.println("完成任务！");
    }
}
