package springbootactivtidemo.controller;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 流程部署
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcessDefinitionTest {
    @Autowired
    RepositoryService repositoryService;

    @Test
    public void deployementProcessDefinition() {
        Deployment deployment = repositoryService.createDeployment()
                .name("测试")
                .addClasspathResource("processes/multipleManager.bpmn")
                .deploy();

        System.out.println("成功部署：" + deployment.getName() + ", ID为：" + deployment.getId());

    }

}
