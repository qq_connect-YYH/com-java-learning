package springbootactivtidemo.controller;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by qin.zl on 2018/9/27.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SelectTasksTest {
    @Autowired
    TaskService taskService;

    @Test
    public void select() {
        List<Task> tasks = taskService.createTaskQuery().taskAssignee("张三,李四,王五").list();
        System.out.println(tasks);

    }
}
