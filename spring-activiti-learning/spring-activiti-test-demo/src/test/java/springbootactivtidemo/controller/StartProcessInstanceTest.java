package springbootactivtidemo.controller;

import org.activiti.bpmn.BpmnAutoLayout;
import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 启动流程
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StartProcessInstanceTest {
    @Autowired
    RuntimeService runtimeService;
    @Autowired
    ProcessEngine processEngine;

    @Test
    public void test() {
        StartEvent startEvent = new StartEvent();
        startEvent.setId("");
        startEvent.setName("");
        startEvent.setDocumentation("");
        ActivitiListener listener = new ActivitiListener();
        List<ActivitiListener> activitiListeners = new ArrayList<>();
        activitiListeners.add(listener);
        startEvent.setExecutionListeners(activitiListeners);
        ExclusiveGateway gateway = new ExclusiveGateway();
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setConditionExpression("");
    }


    @Test
    public void startInstance() {
        Map variables = new HashMap();

        List<String> assigneeList = new ArrayList<String>(); //分配任务的人员
        assigneeList.add("张三");
        assigneeList.add("李四");
        assigneeList.add("王五");
        variables.put("assigneeList", assigneeList);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("multipleManager", variables);

        System.out.println("流程启动：" + processInstance.getBusinessKey() + "，流程ID为：" + processInstance.getProcessInstanceId());
    }

    @Test
    public void testDynamicDeploy() throws Exception {

        //创建bpmn模型
        BpmnModel model = new BpmnModel();
        Process process = new Process();
        model.addProcess(process);
        process.setId("my-process");

        //创建bpmn元素
        process.addFlowElement(createStartEvent());
        process.addFlowElement(createUserTask("task1", "First task", "fred"));
        process.addFlowElement(createUserTask("task2", "Second task", "john"));
        process.addFlowElement(null);
        process.addFlowElement(createEndEvent());

        process.addFlowElement(createSequenceFlow("start", "task1"));
        process.addFlowElement(createSequenceFlow("task1", "task2"));
        process.addFlowElement(createSequenceFlow("task2", "end"));

        // 2.生成BPMN自动布局
        new BpmnAutoLayout(model).execute();

        // 3. 部署这个BPMN模型
        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addBpmnModel("dynamic-model.bpmn", model).name("Dynamic process deployment")
                .deploy();

        // 4. 启动流程实例
        ProcessInstance processInstance = processEngine.getRuntimeService()
                .startProcessInstanceByKey("my-process");

        // 5 发起任务
        List tasks = processEngine.getTaskService().createTaskQuery()
                .processInstanceId(processInstance.getId()).list();

        Assert.assertEquals(1, tasks.size());
//        Assert.assertEquals("First task", tasks.get(0).getName());
//        Assert.assertEquals("fred", tasks.get(0).getAssignee());

        // 6. 保存bpmn流程图
        InputStream processDiagram = processEngine.getRepositoryService()
                .getProcessDiagram(processInstance.getProcessDefinitionId());
//        FileUtils.copyInputStreamToFile(processDiagram, new File("target/diagram.png"));

        // 7. 保存为bpmn.xml的xml类型文件
        InputStream processBpmn = processEngine.getRepositoryService()
                .getResourceAsStream(deployment.getId(), "dynamic-model.bpmn");
//        FileUtils.copyInputStreamToFile(processBpmn,
//                new File("target/process.bpmn20.xml"));
    }

    //创建task
    protected UserTask createUserTask(String id, String name, String assignee) {
        UserTask userTask = new UserTask();
        userTask.setName(name);
        userTask.setId(id);
        userTask.setAssignee(assignee);
        return userTask;
    }

    //创建箭头
    protected SequenceFlow createSequenceFlow(String from, String to) {
        SequenceFlow flow = new SequenceFlow();
        flow.setSourceRef(from);
        flow.setTargetRef(to);
        return flow;
    }

    public StartEvent createStartEvent() {
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        return startEvent;
    }

    protected EndEvent createEndEvent() {
        EndEvent endEvent = new EndEvent();
        endEvent.setId("end");
        return endEvent;
    }
}
