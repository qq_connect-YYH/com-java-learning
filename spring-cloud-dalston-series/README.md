## spring cloud dalston 系列学习
[参考](http://blog.didispace.com/spring-cloud-learning/)

### 一、服务注册与发现

1. 服务注册中心 eureka
- `com.dalston.EurekaServerApplication`

2. 服务提供方
- `com.dalston.EurekaClientApplication`

3. 服务消费方
- `com.dalston.EurekaConsumerApplication`
- 通过 LoadBalancerClient 消费服务，接口 /consumer
- 通过 feign 消费服务，接口 /feign/consumer
- 通过 feign 上传文件，接口 /uploadFile

### 二、分布式配置中心
1. 配置中心服务 config
- `com.config.center.ConfigApplication`
- 可以把该服务注册到服务中心,实现负载均衡

2. 读取配置的客户端
- `com.config.client.ConfigClientApplication`
- 动态改变配置
```$xslt
接口加入注解 @RefreshScope
请求接口 post-> http://ip:port/refresh
```

### 三、服务容错保护（Hystrix服务降级）
1. Hystrix具备了服务降级、服务熔断、线程隔离、请求缓存、请求合并以及服务监控等强大功能。
- `com.hystrix.HystrixApplication`
- 服务降级
```$xslt
通过 @HystrixCommand实现，请求超时会进入，降级的逻辑方法里面
```

- 线程隔离（依赖隔离）
```$xslt
为每一个Hystrix命令创建一个独立的线程池，这样就算某个在Hystrix命令包装下的依赖服务出现延迟过高的情况，也只是对该依赖服务的调用产生影响，而不会拖慢其他的服务。
```

- 服务熔断（断路器）
```$xslt
断路器三个参数：
快照时间窗：断路器确定是否打开需要统计一些请求和错误数据，而统计的时间范围就是快照时间窗，默认为最近的10秒。
请求总数下限：在快照时间窗内，必须满足请求总数下限才有资格根据熔断。默认为20，意味着在10秒内，如果该hystrix命令的调用此时不足20次，即时所有的请求都超时或其他原因失败，断路器都不会打开。
错误百分比下限：当请求总数在快照时间窗内超过了下限，比如发生了30次调用，如果在这30次调用中，有16次发生了超时异常，也就是超过50%的错误百分比，在默认设定50%下限情况下，这时候就会将断路器打开。
```

### 四、服务网关
1. zuul 实现服务网关
- `com.gateway.GatewayApplication`
- 网关注册到服务中心后，会读取服务中心的所有服务，并做路由配置映射。
- 请求方式：`http://ip:网关port/服务名applicationName/接口`

- 网关过滤器，进行安全过滤
```$xslt
com.gateway.filter.GatewayAccessFilter
```


### 五、消息驱动的微服务
1. spring rabbitmq
- `com.stream.StreamApplication`
- 绑定器（Binder）
```$xslt
应用要与消息中间件进行绑定，向应用暴露统一的通道。
```
- 发布-订阅模式
```$xslt
在Spring Cloud Stream中的消息通信方式遵循了发布-订阅模式，当一条消息被投递到消息中间件之后，它会通过共享的Topic主题进行广播，消息消费者在订阅的主题中收到它并触发自身的业务逻辑处理。
Topic 在RabbitMQ中的它对应了Exchange、而在Kakfa中则对应了Kafka中的Topic。
```
- 消费组
```$xslt
不希望同一个消息被集群的应用多次消费，设立一个消费组，在改组下的应用只能有一个应用消费。
通过：spring.cloud.stream.bindings.input.group=xxx  设立组
```
- 消息分区
```$xslt
一类消息的多次发布可能被消费组的不通应用消费，不好做统计请求，这是利用消息分区，指定一个应用消费该类消息。
```


### 六、分布式服务跟踪
1. 一个客户端请求在后台可能会调用多个微服务处理才返回结果，这时候需要一个请求链路的信息，可用 Spring Cloud Sleuth 进行记录。
- [参看](http://blog.didispace.com/spring-cloud-starter-dalston-8-1/)



