package com.config.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by Administrator on 2019/6/26.
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigClientApplication {

    public static void main(String[] args) {
//        SpringApplication.run(ConfigClientApplication.class, args);
        new SpringApplicationBuilder(ConfigClientApplication.class)
                .web(true)
                .run(args);
    }
}
