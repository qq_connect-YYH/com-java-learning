package com.config.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/6/26.
 */
//配置修改刷新
@RefreshScope
@RestController
public class TestController {

    @Value("${mytest}")
    String mytest;

    @GetMapping("/mytest")
    public String getMytest(){
        return mytest;
    }
}
