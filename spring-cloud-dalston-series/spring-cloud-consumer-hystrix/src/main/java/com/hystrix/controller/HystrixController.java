package com.hystrix.controller;

import com.hystrix.service.ConsumerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/6/27.
 */
@Slf4j
@RestController
public class HystrixController {

    @Autowired
    ConsumerService consumerService;


    @GetMapping("/hystrix")
    public String hystrix(){
        log.info("request hystrix");
        return consumerService.consumer();
    }
}
