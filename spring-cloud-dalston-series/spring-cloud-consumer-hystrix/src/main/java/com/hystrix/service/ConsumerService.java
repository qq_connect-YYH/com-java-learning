package com.hystrix.service;

import com.hystrix.feign.DcClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Administrator on 2019/6/27.
 */
@Service
public class ConsumerService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    DcClient dcClient;

    /**
     * 服务熔断
     * consumer 和 fallBack  方法形参要一致
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallBack")
    public String consumer(){
        return dcClient.dc();
//        return restTemplate.getForObject("", String.class);
    }

    //对自身服务起到了基础的保护，同时还为异常情况提供了自动的服务降级切换机制。
    public String fallBack(){
        return "fallBack";
    }
}
