package com.dalston.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2019/6/26.
 */
@Slf4j
@RestController
public class DcController {

    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/dc")
    public String dc() throws InterruptedException {
        //制造服务延迟，判断hystrix
//        Thread.sleep(6000);

        List<String> services = discoveryClient.getServices();
        log.info("services: "+ services);
        return "services: " + services;
    }
}
