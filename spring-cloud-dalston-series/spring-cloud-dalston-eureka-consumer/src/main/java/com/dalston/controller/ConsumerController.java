package com.dalston.controller;

import com.dalston.feign.DcClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Administrator on 2019/6/26.
 */
@Slf4j
@RestController
public class ConsumerController {

    /**
     * 负载均衡客户端
     */
    @Autowired
    LoadBalancerClient loadBalancerClient;
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/consumer")
    public String consumer(){
        ServiceInstance serviceInstance = loadBalancerClient.choose("spring-cloud-dalston-eureka-client");
        String url = "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + "/dc";
        log.info("url "+ url);
        return restTemplate.getForObject(url, String.class);
    }

    //使用feign进行远程调用
    @Autowired
    DcClient dcClient;
    @GetMapping("/feign/consumer")
    public String feignConsumer(){
        //也有负载均衡
        return dcClient.dc();
    }
}
