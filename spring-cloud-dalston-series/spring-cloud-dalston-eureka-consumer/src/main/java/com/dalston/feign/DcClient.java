package com.dalston.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Administrator on 2019/6/26.
 */
@FeignClient("spring-cloud-dalston-eureka-client")
public interface DcClient {

    @GetMapping("/dc")
    String dc();
}
