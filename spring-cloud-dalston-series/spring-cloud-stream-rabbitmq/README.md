### rabbitmq消息学习
```
Spring Cloud Stream是一个用来为微服务应用构建消息驱动能力的框架。
三个核心概念:发布-订阅、消费组、消息分区。

```
1. hello 主题（Topic）
- 通过绑定器（binder）创建在hello上的 生产者和消费者。
```$xslt
位置：com.stream.rabbitmq.hello
测试类： HelloRabbitMqTest
```
- 绑定器（binder）
```$xslt
通过定义绑定器作为中间层，完美地实现了应用程序与消息中间件细节之间的隔离。
通过向应用程序暴露统一的Channel通道，使得应用程序不需要再考虑各种不同的消息中间件实现。
当我们需要升级消息中间件，或是更换其他消息中间件产品时，我们要做的就是更换它们对应的Binder绑定器而不需要修改任何Spring Boot的应用逻辑。
```
- 发布-订阅模式
```$xslt
hello 就是一个主题（topic）的概念，绑定器（binder）通过绑定该主题，很好的隔离了业务和中间件的交互。
我们只需指定该主题下的绑定器相关的输入或输出通道，在进行业务代码的书写。
```

2. 消费组 相关学习
- 主题 minigroup
- 配置组相关信息
```$xslt
位置： com.stream.rabbitmq.minigroup
配置： spring.cloud.stream.bindings.mini-group.group=miniGroup
启动： 至少启动2个实例，看看是不是该消息只发送到一个服务中。

tag: 这边有主题的生产者和消费者，在rabbitmq上发送消息会报错，不晓得为啥？

```
- 输入输出消息通道不通，如何绑定到同一个topic
```$xslt
com.stream.rabbitmq.channel_topic.ChannelTopic
```
- 同一通道根据消息内容分发不同的消费逻辑
```$xslt
http://blog.didispace.com/spring-cloud-starter-finchley-7-6/
```
- 使用延迟消息实现定时任务（RabbitMQ）
```$xslt
http://blog.didispace.com/spring-cloud-starter-finchley-7-7/
```
- 消费失败后的处理策略
```$xslt
http://blog.didispace.com/spring-cloud-starter-finchley-7-2/
```