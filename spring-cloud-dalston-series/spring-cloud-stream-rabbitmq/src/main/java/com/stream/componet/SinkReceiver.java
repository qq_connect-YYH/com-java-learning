package com.stream.componet;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * Created by Administrator on 2019/6/27.
 */
@Slf4j
//用来指定一个或多个定义了@Input或@Output注解的接口，以此实现对消息通道（Channel）的绑定
//对输入消息通道绑定的定义
@EnableBinding(Sink.class)
public class SinkReceiver {

    /**
     * 将被修饰的方法注册为消息中间件上数据流的事件监听器，注解中的属性值对应了监听的消息通道名。
     * 我们通过@StreamListener(Sink.INPUT)注解将receive方法注册为对input消息通道的监听处理器，
     * 所以当我们在RabbitMQ的控制页面中发布消息的时候，receive方法会做出对应的响应动作。
     * @param obj
     */
    @StreamListener(Sink.INPUT)
    public void receive(Object obj){
        log.info("receiver: " + obj);
    }



}
