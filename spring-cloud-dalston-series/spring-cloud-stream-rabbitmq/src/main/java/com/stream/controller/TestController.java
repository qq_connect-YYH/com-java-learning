package com.stream.controller;

import com.stream.rabbitmq.channel_topic.ChannelTopic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@RestController
public class TestController {

    @Autowired
    private ChannelTopic channelTopic;

    @GetMapping("/sendMessage")
    public String messageWithMQ(@RequestParam String message) {
        channelTopic.output().send(MessageBuilder.withPayload(message).build());
        return "ok";
    }
}
