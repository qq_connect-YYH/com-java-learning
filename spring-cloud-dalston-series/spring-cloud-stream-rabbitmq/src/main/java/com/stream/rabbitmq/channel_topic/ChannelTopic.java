package com.stream.rabbitmq.channel_topic;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 消息通道和主题的理解
 * Created by Administrator on 2019/6/28.
 */
public interface ChannelTopic {

    /**
     * 消息通道
     * 配置文件中需要将这2个消息通道绑定到同一个topic
     */
    String input = "ChannelTopicInput";
    String output = "ChannelTopicOutput";

    //绑定到输入消息通道
    @Input(input)
    SubscribableChannel input();

    //绑定到输出消息通道
    @Output(output)
    MessageChannel output();

}
