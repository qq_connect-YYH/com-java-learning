package com.stream.rabbitmq.channel_topic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

/**
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@EnableBinding(ChannelTopic.class)
public class InputReciver {

    @StreamListener(ChannelTopic.input)
    public void receiver(String obj){
        log.info("receiver: "+ obj);
//        throw new RuntimeException("test error");
    }
}
