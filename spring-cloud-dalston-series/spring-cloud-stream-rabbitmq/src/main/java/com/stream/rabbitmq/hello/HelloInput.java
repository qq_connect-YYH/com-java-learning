package com.stream.rabbitmq.hello;

import com.stream.rabbitmq.ConstantChannel;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * 消费通道
 * Created by Administrator on 2019/6/28.
 */
public interface HelloInput {

    String hello = ConstantChannel.HELLO;

    @Input(value = hello)
    SubscribableChannel hello();

}
