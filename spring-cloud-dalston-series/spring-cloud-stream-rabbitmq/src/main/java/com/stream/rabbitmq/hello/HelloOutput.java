package com.stream.rabbitmq.hello;

import com.stream.rabbitmq.ConstantChannel;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * hello 生产者
 * Created by Administrator on 2019/6/28.
 */
public interface HelloOutput {

    String hello = ConstantChannel.HELLO;

    @Output(value = hello)
    MessageChannel hello();

}
