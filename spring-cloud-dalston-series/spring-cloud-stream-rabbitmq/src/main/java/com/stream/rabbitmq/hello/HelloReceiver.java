package com.stream.rabbitmq.hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

/**
 * 消费者
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@EnableBinding({HelloInput.class })
public class HelloReceiver {


    @StreamListener(HelloInput.hello)
    public void hello(Object result){
        log.info("HelloReceiver: " + result.toString());
    }
}
