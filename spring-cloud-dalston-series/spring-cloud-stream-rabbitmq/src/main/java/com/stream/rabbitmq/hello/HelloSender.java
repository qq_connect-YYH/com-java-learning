package com.stream.rabbitmq.hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

/**
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@EnableBinding( {HelloOutput.class})
public class HelloSender {

    @Autowired
    HelloOutput helloOutput;

    public boolean produce(String msg){
        Message<String> build = MessageBuilder.withPayload(String.format("Produce a message from : %s", msg)).build();

        MessageChannel hello = helloOutput.hello();
        boolean send = hello.send(build);
        log.info("send result: "+ send);

        return send;
    }
}
