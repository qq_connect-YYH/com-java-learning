package com.stream.rabbitmq.minigroup;

import com.stream.rabbitmq.ConstantChannel;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created by Administrator on 2019/6/28.
 */
public interface MiniGroupInput {

    //消息通道
    String miniGroup = ConstantChannel.MINI_GROUP;

    @Input(miniGroup)
    SubscribableChannel miniGroup();

}
