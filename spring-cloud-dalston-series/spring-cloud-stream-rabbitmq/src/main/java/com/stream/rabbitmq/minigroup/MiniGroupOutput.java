package com.stream.rabbitmq.minigroup;

import com.stream.rabbitmq.ConstantChannel;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * Created by Administrator on 2019/6/28.
 */
public interface MiniGroupOutput {

    String miniGroup = ConstantChannel.MINI_GROUP;

    @Output(miniGroup)
    MessageChannel miniGroup();

}
