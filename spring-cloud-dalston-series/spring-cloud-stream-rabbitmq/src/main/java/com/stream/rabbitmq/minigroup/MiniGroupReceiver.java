package com.stream.rabbitmq.minigroup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

/**
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@EnableBinding(MiniGroupInput.class)
public class MiniGroupReceiver {

    @StreamListener(MiniGroupInput.miniGroup)
    public void input(Object obj){
        log.info("MiniGroupReceiver "+ obj);
    }

}
