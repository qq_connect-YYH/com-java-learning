package com.stream.rabbitmq.minigroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

/**
 * Created by Administrator on 2019/6/28.
 */
@EnableBinding(MiniGroupOutput.class)
public class MiniGroupSender {

    @Autowired
    MiniGroupOutput miniGroupOutput;

    public boolean miniGroupOutput(String msg){
        MessageChannel messageChannel = miniGroupOutput.miniGroup();
        Message<String> build = MessageBuilder.withPayload(String.format("miniGroupOutput from : %s", msg)).build();

        boolean send = messageChannel.send(build);
        return send;
    }
}
