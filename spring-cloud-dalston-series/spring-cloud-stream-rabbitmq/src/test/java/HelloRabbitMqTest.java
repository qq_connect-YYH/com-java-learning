import com.stream.StreamApplication;
import com.stream.rabbitmq.hello.HelloSender;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StreamApplication.class)
public class HelloRabbitMqTest {

    @Autowired
    HelloSender helloSender;

    @Test
    public void helloTest(){
        boolean produce = helloSender.produce("hello world");
        log.info("结果 "+ produce);
    }
}
