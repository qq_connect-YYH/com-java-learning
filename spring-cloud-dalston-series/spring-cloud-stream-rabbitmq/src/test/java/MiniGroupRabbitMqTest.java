import com.stream.StreamApplication;
import com.stream.rabbitmq.hello.HelloSender;
import com.stream.rabbitmq.minigroup.MiniGroupSender;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/28.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StreamApplication.class)
public class MiniGroupRabbitMqTest {

    @Autowired
    MiniGroupSender miniGroupSender;

    @Test
    public void helloTest(){
        boolean produce = miniGroupSender.miniGroupOutput("hello mini group");
        log.info("结果 "+ produce);
    }
}
