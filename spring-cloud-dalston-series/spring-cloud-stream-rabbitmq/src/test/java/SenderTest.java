import com.stream.StreamApplication;
import com.stream.componet.SinkSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/27.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StreamApplication.class)
public class SenderTest {

    @Autowired
    SinkSender sinkSender;

    @Test
    public void senderTest(){
        //SinkReceiver 也要修改 监听Source.OUTPUT，才能接受
        String payload = sinkSender.timerMessageSource().receive().getPayload();
//        MessageHeaders headers = sinkSender.timerMessageSource().receive().getHeaders();
//        Object replyChannel = headers.getReplyChannel();
        System.out.println(payload);

    }
}
