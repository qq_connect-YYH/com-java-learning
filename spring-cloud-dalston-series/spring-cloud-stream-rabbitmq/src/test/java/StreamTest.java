import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/6/27.
 */
@RunWith(SpringRunner.class)
//@SpringBootTest(classes = StreamApplication.class)
@EnableBinding(value = {StreamTest.SinkSender.class})
public class StreamTest {

    @Autowired
    SinkSender sinkSender;

    @Test
    public void msgTest(){
        sinkSender.output()
                .send(
                        MessageBuilder.withPayload("produce a message ：http://blog.yyh.com")
                                .build()
                );
    }


    interface SinkSender{
        //定义了一个输出通过，而该输出通道的名称为input，与前文中的Sink中定义的消费通道同名，
        //所以这里的单元测试与前文的消费者程序组成了一对生产者与消费者。
        String output = "input";

        @Output(SinkSender.output)
        MessageChannel output();
    }

}
