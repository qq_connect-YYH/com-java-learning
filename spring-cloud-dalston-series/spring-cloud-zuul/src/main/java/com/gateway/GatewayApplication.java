package com.gateway;

import com.gateway.filter.GatewayAccessFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * Created by Administrator on 2019/6/27.
 */
//开启Zuul的功能
@EnableZuulProxy
@SpringCloudApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    /**
     * 网关过滤器，配置对象启动该过滤器
     * @return
     */
    @Bean
    public GatewayAccessFilter gatewayAccessFilter(){
        return new GatewayAccessFilter();
    }
}
