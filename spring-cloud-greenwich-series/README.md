1. 添加eureka服务注册与发现中心
`spring-cloud-eureka-server`

2. 添加网关
`spring-cloud-gateway`

3.服务熔断监控
`spring-cloud-hystrix-dashboard`

4.添加分布式事务全局管理
- [参考](https://www.txlcn.org/zh-cn/docs/preface.html)
- `spring-cloud-tx-tm`

5.公共feign
`spring-cloud-common-feign`

6.客户端和业务模块测试
`spring-cloud-business-demo`
`spring-cloud-eureka-client`



