package com.feign;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Description:
 * Date: 2018/12/25
 *
 * @author ujued
 */
@ComponentScan
@Configuration
//如果该类在其他目录下需要指定 basePackages属性
@EnableFeignClients
public class FeignConfiguration {
}
