package com.feign.client;

import com.feign.constant.ConstantService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Administrator on 2019/7/11.
 */
@FeignClient(value = ConstantService.SPRING_CLOUD_BUSINESS_DEMO, fallback = BusinessDemoFallback.class)
public interface BusinessDemoClient {

    @GetMapping("/rpc")
    String rpc(@RequestParam("value") String value) ;

}
