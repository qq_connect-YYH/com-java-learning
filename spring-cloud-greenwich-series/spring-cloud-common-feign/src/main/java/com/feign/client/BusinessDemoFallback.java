package com.feign.client;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Administrator on 2019/7/11.
 */
@Component
public class BusinessDemoFallback implements BusinessDemoClient {

    @Override
    public String rpc(@RequestParam("value") String value) {
        throw new RuntimeException("rpc 异常");
    }

}
