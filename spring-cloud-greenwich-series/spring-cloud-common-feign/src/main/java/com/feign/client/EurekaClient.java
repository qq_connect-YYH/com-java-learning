package com.feign.client;

import com.feign.constant.ConstantService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Administrator on 2019/7/11.
 */
@FeignClient(value = ConstantService.SPRING_CLOUD_EUREKA_CLIENT, fallback = EurekaClientFallback.class)
public interface EurekaClient {

    @GetMapping("/hello")
    String home();

}
