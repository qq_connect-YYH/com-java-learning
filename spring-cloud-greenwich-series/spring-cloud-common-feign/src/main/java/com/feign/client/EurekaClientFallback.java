package com.feign.client;

import com.feign.constant.ConstantService;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2019/7/11.
 */
@Component
public class EurekaClientFallback implements EurekaClient {

    @Override
    public String home() {
        throw new RuntimeException(String.format("调用：%s服务，失败！", ConstantService.SPRING_CLOUD_EUREKA_CLIENT));
    }

}
