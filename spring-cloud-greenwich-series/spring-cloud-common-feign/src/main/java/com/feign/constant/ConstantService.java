package com.feign.constant;

/**
 *
 * @author Administrator
 * @date 2019/7/11
 */
public class ConstantService {

    public final static String SPRING_CLOUD_EUREKA_CLIENT = "spring-cloud-eureka-client";

    public final static String SPRING_CLOUD_BUSINESS_DEMO = "spring-cloud-business-demo";
}
