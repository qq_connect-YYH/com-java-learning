package com.eureka.controller;

import com.eureka.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/7/11.
 */
@RestController
public class FeignController {

    @Autowired
    DemoService demoService;


    @GetMapping("/txlcn")
    public String execute(@RequestParam("value") String value, @RequestParam(value = "ex", required = false) String exFlag) {
        return demoService.execute(value, exFlag);
    }


}
