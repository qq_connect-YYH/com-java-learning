package com.gateway;

import com.gateway.annotation.EnableDynamicRoute;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * Created by Administrator on 2019/7/3.
 */
//@SpringBootApplication
//@EnableEurekaClient
@SpringCloudApplication
@EnableDynamicRoute
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
