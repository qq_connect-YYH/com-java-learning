package com.gateway.annotation;

import com.gateway.conf.DynamicRouteAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启动态加载的路由,并且会清空配置文件设置的路由
 * 注：这个注解使用前，要确保已使用动态加载策略，不然会没有路由
 * Created by Administrator on 2019/7/3.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(DynamicRouteAutoConfiguration.class)
public @interface EnableDynamicRoute {

}
