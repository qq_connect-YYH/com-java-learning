package com.gateway.conf;

import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.cloud.gateway.config.PropertiesRouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 动态路由配置清空属性配置
 * Created by Administrator on 2019/7/3.
 */
@Configuration
//@ComponentScan("com.gateway.conf")
public class DynamicRouteAutoConfiguration {

    /**
     * 配置文件设置为空
     * redis或其他配置 加载为准
     * @return
     */
    @Bean
    public PropertiesRouteDefinitionLocator propertiesRouteDefinitionLocator() {
        return new PropertiesRouteDefinitionLocator(new GatewayProperties());
    }
}
