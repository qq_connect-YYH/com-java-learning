package com.gateway.support;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 固定的路由加载策略，后续可以使用redis替换
 * Created by Administrator on 2019/7/3.
 */
@Slf4j
@Component
public class FixedRouteDefinitionWriter implements RouteDefinitionRepository {

    private List<RouteDefinition> generateRouteDefinitions() {
        List<RouteDefinition> rd = new ArrayList<>(6);
        RouteDefinition routeDefinition = new RouteDefinition();
        routeDefinition.setId("spring-cloud-eureka-client");
        routeDefinition.setUri(URI.create("lb://spring-cloud-eureka-client"));
        routeDefinition.setOrder(0);

        //所有 /test/** 的请求都会走该服务
        //构造 predicate
        ArrayList<PredicateDefinition> definitions = new ArrayList<>();
        PredicateDefinition pd = new PredicateDefinition();
        pd.setName("Path");
        HashMap<String, String> pdMap = new HashMap<>();
        pdMap.put("_genkey_0", "/test/**");
        pd.setArgs(pdMap);
        definitions.add(pd);

        //构造filters
        List<FilterDefinition> filters = new ArrayList<>();
        FilterDefinition filter = new FilterDefinition();
            //熔断机制,统一处理
        filter.setName("Hystrix");
        HashMap<String, String> fil = new HashMap<>();
        fil.put("name", "default");
        fil.put("fallbackUri", "forward:/fallback");
        filter.setArgs(fil);
        filters.add(filter);

        routeDefinition.setPredicates(definitions);
        routeDefinition.setFilters(filters);
        rd.add(routeDefinition);
        return rd;
    }

    /**
     * 获取所有路由
     * @return
     */
    @Override
    public Flux<RouteDefinition> getRouteDefinitions() {
        List<RouteDefinition> rd = generateRouteDefinitions();
        Flux<RouteDefinition> routeDefinitionFlux = Flux.fromIterable(rd);
        log.debug("路由定义条数： {}， {}", rd.size(), rd);
        return routeDefinitionFlux;
    }



    @Override
    public Mono<Void> save(Mono<RouteDefinition> route) {
        return null;
    }

    @Override
    public Mono<Void> delete(Mono<String> routeId) {
        return null;
    }


}
