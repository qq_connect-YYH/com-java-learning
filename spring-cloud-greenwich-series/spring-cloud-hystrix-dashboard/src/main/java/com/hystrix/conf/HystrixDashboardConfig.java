package com.hystrix.conf;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 注册HystrixMetricsStreamServlet
 * 在2.x之前的版本中，会自动注入该Servlet的，但是在2.x之后的版本，没有自动注册该Servlet。
 * Created by Administrator on 2019/7/4.
 */
//@Configuration
public class HystrixDashboardConfig {

//    @Bean
//    public ServletRegistrationBean servletRegistrationBean(){
//        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
//        ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
//        registrationBean.setLoadOnStartup(1);
//        registrationBean.addUrlMappings("/hystrix.stream");
//        registrationBean.setName("HystrixMetricsStreamServlet");
//        return registrationBean;
//    }
}
